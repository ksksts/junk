// Copyright 2005-2009 Gallio Project - http://www.gallio.org/
// Portions Copyright 2000-2004 Jonathan de Halleux
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// modified by ksksts. see modified-files.diff

using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestExpressionAssertion.Core;

namespace MSTestExpressionAssertionTest.Core
{
    [TestClass]
    public class ExpressionFormatterTest
    {
        public ExpressionFormatterTest() { }

        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        [TestMethod]
        public void TestFormatCurrentMembers()
        {
            Assert.AreEqual("() => ExpressionFormatterTest.staticField", ExpressionFormatter.Format(() => ExpressionFormatterTest.staticField));
            Assert.AreEqual("() => ExpressionFormatterTest.StaticProperty", ExpressionFormatter.Format(() => ExpressionFormatterTest.StaticProperty));
            Assert.AreEqual("() => ExpressionFormatterTest.StaticMethod()", ExpressionFormatter.Format(() => ExpressionFormatterTest.StaticMethod()));
            var expressionFormatterTest = new ExpressionFormatterTest();
            Assert.AreEqual("() => expressionFormatterTest.field", ExpressionFormatter.Format(() => expressionFormatterTest.field));
            Assert.AreEqual("() => expressionFormatterTest.Property", ExpressionFormatter.Format(() => expressionFormatterTest.Property));
            Assert.AreEqual("() => expressionFormatterTest[0]", ExpressionFormatter.Format(() => expressionFormatterTest[0]));
            Assert.AreEqual("() => expressionFormatterTest.Method()", ExpressionFormatter.Format(() => expressionFormatterTest.Method()));
        }

        private static int staticField = 0;
        private static int StaticProperty { get; set; }
        private static void StaticMethod() { }
        private int field = 0;
        private int Property { get; set; }
        private int this[int n] { get { return 0; } set { } }
        private void Method() { }

        [TestMethod]
        public void TestFormatMembers()
        {
            Assert.AreEqual("() => Foo.staticField", ExpressionFormatter.Format(() => Foo.staticField));
            Assert.AreEqual("() => Foo.StaticProperty", ExpressionFormatter.Format(() => Foo.StaticProperty));
            Assert.AreEqual("() => Foo.StaticMethod()", ExpressionFormatter.Format(() => Foo.StaticMethod()));
            var foo = new Foo();
            Assert.AreEqual("() => foo.field", ExpressionFormatter.Format(() => foo.field));
            Assert.AreEqual("() => foo.Property", ExpressionFormatter.Format(() => foo.Property));
            Assert.AreEqual("() => foo[0]", ExpressionFormatter.Format(() => foo[0]));
            Assert.AreEqual("() => foo.Method()", ExpressionFormatter.Format(() => foo.Method()));
        }

        private class Foo
        {
            public static int staticField = 0;
            public static int StaticProperty { get; set; }
            public static void StaticMethod() { }
            public int field = 0;
            public int Property { get; set; }
            public int this[int n] { get { return 0; } set { } }
            public void Method() { }
            public Foo() { }
        }

        [TestMethod]
        public void TestFormatIndexer()
        {
            var list = new List<int>() { 0, };
            Assert.AreEqual("() => list[0]", ExpressionFormatter.Format(() => list[0]));
        }

        [TestMethod]
        public void TestFormatSimpleExpressions()
        {
            int x = 5, y = 2;
            int[] arr = new int[1];
            UnaryPlusType z = new UnaryPlusType();

            // binary operators
            Assert.AreEqual("() => x + y", ExpressionFormatter.Format(() => x + y));
            Assert.AreEqual("() => checked(x + y)", ExpressionFormatter.Format(() => checked(x + y)));
            Assert.AreEqual("() => x & y", ExpressionFormatter.Format(() => x & y));
            Assert.AreEqual("() => true && x == 5", ExpressionFormatter.Format(() => true && x == 5));
            Assert.AreEqual("() => arr[0]", ExpressionFormatter.Format(() => arr[0]));
            Assert.AreEqual("() => arr ?? arr", ExpressionFormatter.Format(() => arr ?? arr));
            Assert.AreEqual("() => x / y", ExpressionFormatter.Format(() => x / y));
            Assert.AreEqual("() => x == y", ExpressionFormatter.Format(() => x == y));
            Assert.AreEqual("() => x ^ y", ExpressionFormatter.Format(() => x ^ y));
            Assert.AreEqual("() => x > y", ExpressionFormatter.Format(() => x > y));
            Assert.AreEqual("() => x >= y", ExpressionFormatter.Format(() => x >= y));
            Assert.AreEqual("() => ((Func<int>) (() => 5))()", ExpressionFormatter.Format(() => ((Func<int>) (() => 5))()));
            Assert.AreEqual("() => x << y", ExpressionFormatter.Format(() => x << y));
            Assert.AreEqual("() => x < y", ExpressionFormatter.Format(() => x < y));
            Assert.AreEqual("() => x <= y", ExpressionFormatter.Format(() => x <= y));
            Assert.AreEqual("() => x % y", ExpressionFormatter.Format(() => x % y));
            Assert.AreEqual("() => x * y", ExpressionFormatter.Format(() => x * y));
            Assert.AreEqual("() => checked(x * y)", ExpressionFormatter.Format(() => checked(x * y)));
            Assert.AreEqual("() => x != y", ExpressionFormatter.Format(() => x != y));
            Assert.AreEqual("() => x | y", ExpressionFormatter.Format(() => x | y));
            Assert.AreEqual("() => false || x == 5", ExpressionFormatter.Format(() => false || x == 5));
            Assert.AreEqual("() => 3 ** 4",
                            ExpressionFormatter.Format(Expression.Lambda<Func<double>>(Expression.Power(Expression.Constant(3.0), Expression.Constant(4.0)))));
            Assert.AreEqual("() => x >> y", ExpressionFormatter.Format(() => x >> y));
            Assert.AreEqual("() => x - y", ExpressionFormatter.Format(() => x - y));
            Assert.AreEqual("() => checked(x - y)", ExpressionFormatter.Format(() => checked(x - y)));

            // call
            Assert.AreEqual("() => arr.ToString()", ExpressionFormatter.Format(() => arr.ToString()));

            // conditional
            Assert.AreEqual("() => x == 3 ? 1 : 2", ExpressionFormatter.Format(() => x == 3 ? 1 : 2));

            // lambda (done elsewhere)

            // list init
            Assert.AreEqual("() => new List<int>() { 1, 2, 3 }", ExpressionFormatter.Format(() => new List<int> {1, 2, 3}));

            // member init
            Assert.AreEqual("() => new ExpressionFormatterTest.MemberInitType() { Bar = 42, List = { 1, 2, 3 }, Aggregate = { Foo = 42 } }",
                            ExpressionFormatter.Format(() => new MemberInitType { Bar = 42, List = { 1, 2, 3 }, Aggregate = { Foo = 42 } }));

            // member access (done elsewhere)

            // new
            Assert.AreEqual("() => new ExpressionFormatterTest.MemberInitType()", ExpressionFormatter.Format(() => new MemberInitType()));

            // new array bounds
            Assert.AreEqual("() => new int[3]", ExpressionFormatter.Format(() => new int[3]));

            // new array init
            Assert.AreEqual("() => new int[] { 1, 2, 3 }", ExpressionFormatter.Format(() => new int[] {1, 2, 3}));

            // parameter
            Assert.AreEqual("i => i == 2", ExpressionFormatter.Format((int i) => i == 2));

            // quote
            Assert.AreEqual("() => this.AssertFormat(\"() => x == 5\", (() => x == 5))",
                            ExpressionFormatter.Format(() => AssertFormat("() => x == 5", () => x == 5)));

            // type binary
            Assert.AreEqual("() => (object) x is int", ExpressionFormatter.Format(() => (object) x is int));

            // unary
            Assert.AreEqual("() => arr.Length", ExpressionFormatter.Format(() => arr.Length));
            Assert.AreEqual("() => (double) x", ExpressionFormatter.Format(() => (double) x));
            Assert.AreEqual("() => checked((double) x)", ExpressionFormatter.Format(() => checked((double)x)));
            Assert.AreEqual("() => - x", ExpressionFormatter.Format(() => -x));
            Assert.AreEqual("() => checked(- x)", ExpressionFormatter.Format(() => checked(-x)));
            Assert.AreEqual("() => ~ x", ExpressionFormatter.Format(() => ~x));
            Assert.AreEqual("() => x as object", ExpressionFormatter.Format(() => x as object));
            Assert.AreEqual("() => + z", ExpressionFormatter.Format(() => +z));
        }

        [TestMethod]
        public void TestFormatPrecedence()
        {
            int x = 5, y = 2;
            Assert.AreEqual("() => x + y * x", ExpressionFormatter.Format(() => x + y * x));
            Assert.AreEqual("() => (x + y) * x", ExpressionFormatter.Format(() => (x + y) * x));
            Assert.AreEqual("() => y * x + y", ExpressionFormatter.Format(() => y * x + y));
            Assert.AreEqual("() => y * (x + y)", ExpressionFormatter.Format(() => y * (x + y)));
        }

        [TestMethod]
        public void TestFormatCheckedAndUnchecked()
        {
            int x = 5, y = 2;
            Assert.AreEqual("() => checked(x + y + x)", ExpressionFormatter.Format(() => checked(x + y + x)));
            Assert.AreEqual("() => checked(x + unchecked(y + x))", ExpressionFormatter.Format(() => checked(x + unchecked(y + x))));
            Assert.AreEqual("() => checked(x + unchecked(y + x) * x)", ExpressionFormatter.Format(() => checked(x + unchecked(y + x) * x)));
            Assert.AreEqual("() => checked(x + unchecked(y + x) * x) + y", ExpressionFormatter.Format(() => checked(x + unchecked(y + x) * x) + y));
            Assert.AreEqual("() => checked(x + unchecked(y + checked(x * x)) * x) + y", ExpressionFormatter.Format(() => checked(x + unchecked(y + checked(x * x)) * x) + y));
        }

        private void AssertFormat<T>(string str, Expression<Func<T>> expr) { }

        private struct UnaryPlusType
        {
            public static UnaryPlusType operator +(UnaryPlusType x)
            {
                return x;
            }
        }

        private class MemberInitType
        {
            public int Bar = 0;
            public AggregateType Aggregate = new AggregateType();
            public List<int> List = new List<int>();
        }

        private class AggregateType
        {
            public int Foo = 0;
        }
    }
}