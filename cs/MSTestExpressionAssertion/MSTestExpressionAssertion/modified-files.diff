--- reference\Gallio\Gallio\Runtime\Formatting\TypeFormattingRule.cs	2009-09-15 21:08:14.609375000 +0900
+++ Core\TypeFormatter.cs	2010-01-22 23:21:08.515625000 +0900
@@ -1,4 +1,4 @@
-// Copyright 2005-2009 Gallio Project - http://www.gallio.org/
+﻿// Copyright 2005-2009 Gallio Project - http://www.gallio.org/
 // Portions Copyright 2000-2004 Jonathan de Halleux
 // 
 // Licensed under the Apache License, Version 2.0 (the "License");
@@ -13,47 +13,36 @@
 // See the License for the specific language governing permissions and
 // limitations under the License.
 
+// modified by ksksts. see modified-files.diff
+
 using System;
 using System.Text;
 
-namespace Gallio.Runtime.Formatting
+namespace MSTestExpressionAssertion.Core
 {
-    /// <summary>
-    /// A formatting rule for <see cref="Type" />.
-    /// </summary>
-    /// <remarks>
-    /// <para>
-    /// Formats values like: string, MyType.Nested, int[], byte*, similar to C#.
-    /// </para>
-    /// </remarks>
-    public sealed class TypeFormattingRule : IFormattingRule
+    public class TypeFormatter
     {
-        /// <inheritdoc />
-        public int? GetPriority(Type type)
+        public static string Format(Type type)
         {
-            if (typeof(Type).IsAssignableFrom(type))
-                return FormattingRulePriority.Best;
-            return null;
+            return Format(type, false, true);
         }
 
-        /// <inheritdoc />
-        public string Format(object obj, IFormatter formatter)
+        public static string Format(Type type, bool @namespace, bool builtInTypeName)
         {
-            var value = (Type) obj;
             var result = new StringBuilder();
-            AppendType(result, value);
+            AppendType(result, @namespace, builtInTypeName, type);
             return result.ToString();
         }
 
-        private static void AppendType(StringBuilder result, Type type)
+        private static void AppendType(StringBuilder result, bool @namespace, bool builtInTypeName, Type type)
         {
             if (type.HasElementType)
             {
-                AppendType(result, type.GetElementType());
+                AppendType(result, @namespace, builtInTypeName, type.GetElementType());
 
                 if (type.IsArray)
                 {
-                    int rank = type.GetArrayRank();
+                    var rank = type.GetArrayRank();
                     result.Append('[');
                     result.Append(',', rank - 1);
                     result.Append(']');
@@ -70,17 +59,17 @@
                 return;
             }
 
-            string typeName = GetBuiltInTypeName(type);
+            var typeName = builtInTypeName ? GetBuiltInTypeName(type) : null;
             if (typeName == null)
             {
                 if (type.IsNested)
                 {
-                    AppendType(result, type.DeclaringType);
+                    AppendType(result, @namespace, builtInTypeName, type.DeclaringType);
                     result.Append('.');
                 }
-                else
+                else if (@namespace)
                 {
-                    string typeNamespace = type.Namespace;
+                    var typeNamespace = type.Namespace;
                     if (!string.IsNullOrEmpty(typeNamespace))
                     {
                         result.Append(typeNamespace);
@@ -96,19 +85,19 @@
             if (type.IsGenericType)
             {
                 // strip off the `# part of a generic type name like Dictionary`2
-                int backTickPos = typeName.IndexOf('`');
+                var backTickPos = typeName.IndexOf('`');
                 if (backTickPos >= 0)
                     result.Length -= typeName.Length - backTickPos;
 
                 result.Append('<');
 
-                Type[] typeParams = type.GetGenericArguments();
-                for (int i = 0; i < typeParams.Length; i++)
+                var typeParams = type.GetGenericArguments();
+                for (var i = 0; i < typeParams.Length; i++)
                 {
                     if (i != 0)
                         result.Append(',');
-                    if (! typeParams[i].IsGenericParameter)
-                        AppendType(result, typeParams[i]);
+                    if (!typeParams[i].IsGenericParameter)
+                        AppendType(result, @namespace, builtInTypeName, typeParams[i]);
                 }
 
                 result.Append('>');
@@ -154,4 +143,4 @@
             }
         }
     }
-}
\ No newline at end of file
+}
--- reference\Gallio\Gallio35\Runtime\Formatting\AssertionConditionEvaluator.cs	1970-01-01 09:00:00.000000000 +0900
+++ Core\ExpressionInterpreter.cs	2010-01-23 10:08:23.203125000 +0900
@@ -0,0 +1,127 @@
+// Copyright 2005-2009 Gallio Project - http://www.gallio.org/
+// Portions Copyright 2000-2004 Jonathan de Halleux
+// 
+// Licensed under the Apache License, Version 2.0 (the "License");
+// you may not use this file except in compliance with the License.
+// You may obtain a copy of the License at
+// 
+//     http://www.apache.org/licenses/LICENSE-2.0
+// 
+// Unless required by applicable law or agreed to in writing, software
+// distributed under the License is distributed on an "AS IS" BASIS,
+// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+// See the License for the specific language governing permissions and
+// limitations under the License.
+
+// modified by ksksts. see modified-files.diff
+
+using System;
+using System.Collections.Generic;
+using System.Diagnostics;
+using System.Linq.Expressions;
+using Gallio.Common;
+using Gallio.Common.Linq;
+
+namespace MSTestExpressionAssertion.Core
+{
+    public class ExpressionInterpreter<TResult> : ExpressionInstrumentor
+    {
+        private Trace currentTrace;
+
+        public Expression<Func<TResult>> Condition { get; private set; }
+        public TResult Result { get; private set; }
+        public Trace RootTrace { get; private set; }
+        public List<Triple<int, string, object>> LabeledValues { get; private set; }
+
+        public ExpressionInterpreter() { }
+
+        public TResult Eval(Expression<Func<TResult>> condition)
+        {
+            currentTrace = new Trace(null);
+            Condition = condition;
+            Result = default(TResult);
+            RootTrace = currentTrace;
+            LabeledValues = new List<Triple<int, string, object>>();
+
+            if (condition == null)
+                throw new ArgumentNullException("condition");
+
+            Result = Compile(condition)();
+            Describe(currentTrace);
+
+            RootTrace = RootTrace.Children[0];
+            return Result;
+        }
+
+        [DebuggerHidden, DebuggerStepThrough]
+        protected override T Intercept<T>(Expression expr, Func<T> continuation)
+        {
+            var parentTrace = currentTrace;
+            var trace = new Trace(expr);
+
+            currentTrace.AddChild(trace);
+            currentTrace = trace;
+
+            var result = base.Intercept(expr, continuation);
+            trace.Result = result;
+
+            currentTrace = parentTrace;
+
+            return result;
+        }
+
+        private void Describe(Trace trace)
+        {
+            // Skip trivial nodes in the trace tree that are not of much interest.
+            while (IsTrivialExpression(trace.Expression) && trace.Children.Count == 1)
+                trace = trace.Children[0];
+
+            var labeledTraces = new List<Pair<int, Trace>>();
+            AddLabeledTraces(labeledTraces, 0, trace);
+
+            LabeledValues.Clear();
+            foreach (var labeledTrace in labeledTraces)
+            {
+                var level = labeledTrace.First;
+                var label = ExpressionFormatter.Format(labeledTrace.Second.Expression);
+                var result = labeledTrace.Second.Result;
+                LabeledValues.Add(new Triple<int, string, object>(level, label, result));
+            }
+        }
+
+        private static void AddLabeledTraces(List<Pair<int, Trace>> labeledTraces, int level, Trace trace)
+        {
+            var expr = trace.Expression;
+            if (level == 0 || !(expr is ConstantExpression))
+            {
+                labeledTraces.Add(new Pair<int, Trace>(level, trace));
+                foreach (var child in trace.Children)
+                    AddLabeledTraces(labeledTraces, level + 1, child);
+            }
+        }
+
+        private static bool IsTrivialExpression(Expression expr)
+        {
+            return expr == null || expr.NodeType == ExpressionType.Not;
+        }
+
+
+        public sealed class Trace
+        {
+            public Trace(Expression expression)
+            {
+                Expression = expression;
+                Children = new List<Trace>();
+            }
+
+            public Expression Expression { get; private set; }
+            public object Result { get; set; }
+            public IList<Trace> Children { get; private set; }
+
+            public void AddChild(Trace child)
+            {
+                Children.Add(child);
+            }
+        }
+    }
+}
--- reference\Gallio\Gallio35\Runtime\Formatting\ExpressionFormattingRule.cs	2009-09-15 21:07:46.437500000 +0900
+++ Core\ExpressionFormatter.cs	2010-01-22 23:24:26.546875000 +0900
@@ -13,6 +13,8 @@
 // See the License for the specific language governing permissions and
 // limitations under the License.
 
+// modified by ksksts. see modified-files.diff
+
 using System;
 using System.Collections.Generic;
 using System.Collections.ObjectModel;
@@ -23,85 +25,84 @@
 using Gallio.Common;
 using Gallio.Common.Linq;
 
-namespace Gallio.Runtime.Formatting
+namespace MSTestExpressionAssertion.Core
 {
-    /// <summary>
-    /// A formatting rule for <see cref="Expression" />.
-    /// </summary>
-    /// <remarks>
-    /// <para>
-    /// Formats expression trees using a more familiar C#-like syntax than
-    /// the default.  Also recognizes captured variables and displays them
-    /// naturally to conceal the implied field access to an anonymous class.
-    /// </para>
-    /// <para>
-    /// Made-up syntax for nodes that cannot be directly represented in C#.
-    /// <list type="bullet">
-    /// <item>Power operator: **, as in a ** b</item>
-    /// <item>Quote expression: `...`, as in `a + b`</item>
-    /// <item>Constants: formatted recursively using other formatters, which may yield unusual syntax</item>
-    /// </list>
-    /// </para>
-    /// </remarks>
-    public sealed class ExpressionFormattingRule : IFormattingRule
+    public class ExpressionFormatter
     {
-        /// <inheritdoc />
-        public int? GetPriority(Type type)
+        public static string Format(Expression<Action> expression)
         {
-            if (typeof(Expression).IsAssignableFrom(type))
-                return FormattingRulePriority.Best;
-            return null;
+            return Format((Expression)expression);
         }
 
-        /// <inheritdoc />
-        public string Format(object obj, IFormatter formatter)
+        public static string Format<T>(Expression<Action<T>> expression)
         {
-            Expression value = (Expression)obj;
-            ExpressionFormatter visitor = new ExpressionFormatter(formatter);
-            visitor.Visit(value);
-            return visitor.ToString();
+            return Format((Expression)expression);
         }
 
-        // According to C# language spec, section 7.2.1
-        private enum Precedence
+        public static string Format<TResult>(Expression<Func<TResult>> expression)
         {
-            Primary, // x.y  f(x)  a[x]  x++  x--  new  typeof  checked  unchecked
-            Unary, // +  -  !  ~  ++x  --x  (T)x
-            Multiplicative, // *  /  %
-            Additive, // +  -
-            Shift, // <<  >>
-            Relational, // <  >  <=  >=  is  as
-            Equality, // ==  !=
-            LogicalAnd, // &
-            LogicalXor, // ^
-            LogicalOr, // |
-            ConditionalAnd, // &&
-            ConditionalOr, // ||
-            Conditional, // ?:
-            Assignment, // =  *=  /=  %=  +=  -=  <<=  >>=  &=  ^=  |= ??
-            Lambda
+            return Format((Expression)expression);
         }
 
-        private enum CheckingMode
+        public static string Format<T, TResult>(Expression<Func<T, TResult>> expression)
         {
-            Inherit,
-            Checked,
-            Unchecked,
-            Reset
+            return Format((Expression)expression);
         }
 
-        private sealed class ExpressionFormatter : ExpressionVisitor<Unit>
+        public static string Format(Expression expression)
         {
-            private readonly IFormatter formatter;
+            var formatter = new Formatter();
+            var result = formatter.Format(expression);
+            return result;
+        }
+
+        private class Formatter : ExpressionVisitor<Unit>
+        {
+            // According to C# language spec, section 7.2.1
+            private enum Precedence
+            {
+                Primary, // x.y  f(x)  a[x]  x++  x--  new  typeof  checked  unchecked
+                Unary, // +  -  !  ~  ++x  --x  (T)x
+                Multiplicative, // *  /  %
+                Additive, // +  -
+                Shift, // <<  >>
+                Relational, // <  >  <=  >=  is  as
+                Equality, // ==  !=
+                LogicalAnd, // &
+                LogicalXor, // ^
+                LogicalOr, // |
+                ConditionalAnd, // &&
+                ConditionalOr, // ||
+                Conditional, // ?:
+                Assignment, // =  *=  /=  %=  +=  -=  <<=  >>=  &=  ^=  |= ??
+                Lambda
+            }
+
+            private enum CheckingMode
+            {
+                Inherit,
+                Checked,
+                Unchecked,
+                Reset
+            }
+
             private readonly StringBuilder result;
             private readonly Stack<Pair<Precedence, CheckingMode>> modeStack;
 
-            public ExpressionFormatter(IFormatter formatter)
+            public Formatter()
             {
-                this.formatter = formatter;
                 result = new StringBuilder();
                 modeStack = new Stack<Pair<Precedence, CheckingMode>>();
+            }
+
+            public string Format(Expression expression)
+            {
+                result.Remove(0, result.Length);
+                modeStack.Clear();
                 modeStack.Push(new Pair<Precedence, CheckingMode>(Precedence.Lambda, CheckingMode.Reset));
+
+                Visit(expression);
+                return result.ToString();
             }
 
             public void VisitAndQuoteExpressionIfNeeded(Expression expr)
@@ -277,10 +278,18 @@
             {
                 BeginExpression(Precedence.Primary, CheckingMode.Inherit);
 
-                AppendMemberAccess(expr.Object, expr.Method);
-                result.Append('.');
-                result.Append(expr.Method.Name);
-                AppendArguments(expr.Arguments);
+                if (expr.IsIndexer())
+                {
+                    AppendMemberAccess(expr.Object, expr.Method);
+                    AppendIndices(expr.Arguments);
+                }
+                else
+                {
+                    AppendMemberAccess(expr.Object, expr.Method);
+                    result.Append('.');
+                    result.Append(expr.Method.Name);
+                    AppendArguments(expr.Arguments);
+                }
 
                 EndExpression();
                 return Unit.Value;
@@ -305,18 +314,11 @@
                 BeginExpression(Precedence.Primary, CheckingMode.Inherit);
 
                 if (expr.Value is Expression)
-                {
                     VisitAndQuoteExpressionIfNeeded((Expression)expr.Value);
-                }
+                else if (expr.IsThis())
+                    result.Append("this");
                 else
-                {
-                    bool isType = expr.Value is Type;
-                    if (isType)
-                        result.Append("typeof(");
-                    result.Append(formatter.Format(expr.Value));
-                    if (isType)
-                        result.Append(')');
-                }
+                    result.Append(FormatConstantValue(expr));
 
                 EndExpression();
                 return Unit.Value;
@@ -361,10 +363,12 @@
             {
                 BeginExpression(Precedence.Primary, CheckingMode.Inherit);
 
-                if (! expr.IsCapturedVariable())
+                if (!expr.IsLocalMember())
+                {
                     AppendMemberAccess(expr.Expression, expr.Member);
-                else
-                    result.Append(expr.Member.Name);
+                    result.Append('.');
+                }
+                result.Append(expr.Member.Name);
 
                 EndExpression();
                 return Unit.Value;
@@ -387,7 +391,7 @@
                 BeginExpression(Precedence.Primary, CheckingMode.Inherit);
 
                 result.Append("new ");
-                result.Append(expr.Constructor.DeclaringType.Name);
+                result.Append(FormatTypeName(expr.Constructor.DeclaringType));
                 AppendArguments(expr.Arguments);
 
                 EndExpression();
@@ -458,7 +462,7 @@
                 else if (AreParenthesesNeeded(outerPrecedence, innerPrecedence))
                     result.Append('(');
 
-                modeStack.Push(new Pair<Precedence,CheckingMode>(innerPrecedence, innerMode));
+                modeStack.Push(new Pair<Precedence, CheckingMode>(innerPrecedence, innerMode));
             }
 
             private void EndExpression()
@@ -488,6 +492,13 @@
                 result.Append(')');
             }
 
+            private void AppendIndices(ReadOnlyCollection<Expression> args)
+            {
+                result.Append('[');
+                AppendExpressionList(args);
+                result.Append(']');
+            }
+
             private void AppendParameters(ReadOnlyCollection<ParameterExpression> @params)
             {
                 if (@params.Count != 1)
@@ -575,9 +586,20 @@
                 result.Append(" }");
             }
 
+            private string FormatConstantValue(ConstantExpression expr)
+            {
+                if (expr.Value is Type)
+                    return string.Format("typeof({0})", FormatTypeName((Type)expr.Value));
+                if (expr.Value is bool)
+                    return (bool)expr.Value ? "true" : "false";
+                if (expr.Value is char)
+                    return string.Format("'{0}'", (char)expr.Value);
+                return expr.ToString();
+            }
+
             private string FormatTypeName(Type type)
             {
-                return formatter.Format(type);
+                return TypeFormatter.Format(type);
             }
 
             private static bool? DetermineCheckingModeSwitch(CheckingMode outerMode, CheckingMode innerMode)
