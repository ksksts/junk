﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace IListEnumerationBenchmark
{
    class Program
    {
        static void Main(string[] args)
        {
            var size = 1000000;
            var count = 10;
            var sw = new Stopwatch();

            Action<IList<int>> run = list =>
            {
                var useIndexerTicks = 0L;
                var useEnumeratorTicks = 0L;
                var useForeachTicks = 0L;

                for (var c = 0; c < count; c++)
                {
                    var random = new Random(c);
                    for (var n = 0; n < list.Count; n++)
                        list[n] = random.Next(int.MinValue, int.MaxValue);

                    // use indexer
                    sw.Reset();
                    sw.Start();
                    var useIndexerResult = UseIndexer(list);
                    sw.Stop();
                    useIndexerTicks += sw.ElapsedTicks;

                    // use enumerator
                    sw.Reset();
                    sw.Start();
                    var useEnumeratorResult = UseEnumerator(list);
                    sw.Stop();
                    useEnumeratorTicks += sw.ElapsedTicks;

                    // use foreach
                    sw.Reset();
                    sw.Start();
                    var useForeachResult = UseForeach(list);
                    sw.Stop();
                    useForeachTicks += sw.ElapsedTicks;

                    // correctness
                    if (useIndexerResult != useForeachResult)
                        Console.WriteLine("useIndexerResult: {0}, useForeachResult: {1}", useIndexerResult, useForeachResult);
                    if (useEnumeratorResult != useForeachResult)
                        Console.WriteLine("useEnumeratorResult: {0}, useForeachResult: {1}", useEnumeratorResult, useForeachResult);
                }

                Console.WriteLine("use indexer: {0} ({1:F})", useIndexerTicks / count, useIndexerTicks / (double)useForeachTicks);
                Console.WriteLine("use enumerator: {0} ({1:F})", useEnumeratorTicks / count, useEnumeratorTicks / (double)useForeachTicks);
                Console.WriteLine("use foreach: {0} ({1:F})", useForeachTicks / count, useForeachTicks / (double)useForeachTicks);
                Console.WriteLine();
            };

            // int[]
            {
                Console.WriteLine("int[]: ");
                var array = new int[size];
                run(array);
            }

            // List<int>
            {
                Console.WriteLine("List<int>: ");
                var list = new List<int>();
                for (var n = 0; n < size; n++)
                    list.Add(0);
                run(list);
            }
        }

        static int UseIndexer(IList<int> list)
        {
            var result = 0;
            for (var n = 0; n < list.Count; n++)
                result = unchecked(result + list[n]);
            return result;
        }

        static int UseEnumerator(IList<int> list)
        {
            var result = 0;
            var e = list.GetEnumerator();
            while (e.MoveNext())
                result = unchecked(result + e.Current);
            return result;
        }

        static int UseForeach(IList<int> list)
        {
            var result = 0;
            foreach (var x in list)
                result = unchecked(result + x);
            return result;
        }
    }
}
