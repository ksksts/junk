﻿// Copyright 2005-2009 Gallio Project - http://www.gallio.org/
// Portions Copyright 2000-2004 Jonathan de Halleux
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// modified by ksksts. see modified-files.diff

using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestExpressionAssertion.Core;

namespace MSTestExpressionAssertionTest.Core
{
    [TestClass()]
    public class TypeFormatterTest
    {
        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        [TestMethod()]
        public void Format()
        {
            Assert.AreEqual("bool", TypeFormatter.Format(typeof(bool)));
            Assert.AreEqual("char", TypeFormatter.Format(typeof(char)));
            Assert.AreEqual("sbyte", TypeFormatter.Format(typeof(sbyte)));
            Assert.AreEqual("byte", TypeFormatter.Format(typeof(byte)));
            Assert.AreEqual("short", TypeFormatter.Format(typeof(short)));
            Assert.AreEqual("ushort", TypeFormatter.Format(typeof(ushort)));
            Assert.AreEqual("int", TypeFormatter.Format(typeof(int)));
            Assert.AreEqual("uint", TypeFormatter.Format(typeof(uint)));
            Assert.AreEqual("long", TypeFormatter.Format(typeof(long)));
            Assert.AreEqual("ulong", TypeFormatter.Format(typeof(ulong)));
            Assert.AreEqual("float", TypeFormatter.Format(typeof(float)));
            Assert.AreEqual("double", TypeFormatter.Format(typeof(double)));
            Assert.AreEqual("decimal", TypeFormatter.Format(typeof(decimal)));
            Assert.AreEqual("string", TypeFormatter.Format(typeof(string)));
            Assert.AreEqual("object", TypeFormatter.Format(typeof(object)));
            Assert.AreEqual("string[]", TypeFormatter.Format(typeof(string[])));
            Assert.AreEqual("string[,]", TypeFormatter.Format(typeof(string[,])));
            Assert.AreEqual("Dictionary<,>", TypeFormatter.Format(typeof(Dictionary<,>)));
            Assert.AreEqual("Dictionary<int,string>", TypeFormatter.Format(typeof(Dictionary<int, string>)));
            Assert.AreEqual("Dictionary<,>.Enumerator<,>", TypeFormatter.Format(typeof(Dictionary<,>.Enumerator)));

            var anonymousEmpty = new { };
            Assert.AreEqual("<>f__AnonymousType0", TypeFormatter.Format(anonymousEmpty.GetType()));
            var anonymousIntString = new { ID = 0, Name = "anonymous" };
            Assert.AreEqual("<>f__AnonymousType1<int,string>", TypeFormatter.Format(anonymousIntString.GetType()));
        }

        [TestMethod()]
        public void FormatWithOptions()
        {
            var @namespace = default(bool);
            var builtInTypeName = default(bool);
            var anonymousEmpty = new { };
            var anonymousIntString = new { ID = 0, Name = "anonymous" };

            // not including namespace, using built in type name
            @namespace = false;
            builtInTypeName = true;
            Assert.AreEqual("bool", TypeFormatter.Format(typeof(bool), @namespace, builtInTypeName));
            Assert.AreEqual("char", TypeFormatter.Format(typeof(char), @namespace, builtInTypeName));
            Assert.AreEqual("sbyte", TypeFormatter.Format(typeof(sbyte), @namespace, builtInTypeName));
            Assert.AreEqual("byte", TypeFormatter.Format(typeof(byte), @namespace, builtInTypeName));
            Assert.AreEqual("short", TypeFormatter.Format(typeof(short), @namespace, builtInTypeName));
            Assert.AreEqual("ushort", TypeFormatter.Format(typeof(ushort), @namespace, builtInTypeName));
            Assert.AreEqual("int", TypeFormatter.Format(typeof(int), @namespace, builtInTypeName));
            Assert.AreEqual("uint", TypeFormatter.Format(typeof(uint), @namespace, builtInTypeName));
            Assert.AreEqual("long", TypeFormatter.Format(typeof(long), @namespace, builtInTypeName));
            Assert.AreEqual("ulong", TypeFormatter.Format(typeof(ulong), @namespace, builtInTypeName));
            Assert.AreEqual("float", TypeFormatter.Format(typeof(float), @namespace, builtInTypeName));
            Assert.AreEqual("double", TypeFormatter.Format(typeof(double), @namespace, builtInTypeName));
            Assert.AreEqual("decimal", TypeFormatter.Format(typeof(decimal), @namespace, builtInTypeName));
            Assert.AreEqual("string", TypeFormatter.Format(typeof(string), @namespace, builtInTypeName));
            Assert.AreEqual("object", TypeFormatter.Format(typeof(object), @namespace, builtInTypeName));
            Assert.AreEqual("string[]", TypeFormatter.Format(typeof(string[]), @namespace, builtInTypeName));
            Assert.AreEqual("string[,]", TypeFormatter.Format(typeof(string[,]), @namespace, builtInTypeName));
            Assert.AreEqual("Dictionary<,>", TypeFormatter.Format(typeof(Dictionary<,>), @namespace, builtInTypeName));
            Assert.AreEqual("Dictionary<int,string>", TypeFormatter.Format(typeof(Dictionary<int, string>), @namespace, builtInTypeName));
            Assert.AreEqual("Dictionary<,>.Enumerator<,>", TypeFormatter.Format(typeof(Dictionary<,>.Enumerator), @namespace, builtInTypeName));
            Assert.AreEqual("<>f__AnonymousType0", TypeFormatter.Format(anonymousEmpty.GetType(), @namespace, builtInTypeName));
            Assert.AreEqual("<>f__AnonymousType1<int,string>", TypeFormatter.Format(anonymousIntString.GetType(), @namespace, builtInTypeName));

            // including namespace, using built in type name
            @namespace = true;
            builtInTypeName = true;
            Assert.AreEqual("bool", TypeFormatter.Format(typeof(bool), @namespace, builtInTypeName));
            Assert.AreEqual("char", TypeFormatter.Format(typeof(char), @namespace, builtInTypeName));
            Assert.AreEqual("sbyte", TypeFormatter.Format(typeof(sbyte), @namespace, builtInTypeName));
            Assert.AreEqual("byte", TypeFormatter.Format(typeof(byte), @namespace, builtInTypeName));
            Assert.AreEqual("short", TypeFormatter.Format(typeof(short), @namespace, builtInTypeName));
            Assert.AreEqual("ushort", TypeFormatter.Format(typeof(ushort), @namespace, builtInTypeName));
            Assert.AreEqual("int", TypeFormatter.Format(typeof(int), @namespace, builtInTypeName));
            Assert.AreEqual("uint", TypeFormatter.Format(typeof(uint), @namespace, builtInTypeName));
            Assert.AreEqual("long", TypeFormatter.Format(typeof(long), @namespace, builtInTypeName));
            Assert.AreEqual("ulong", TypeFormatter.Format(typeof(ulong), @namespace, builtInTypeName));
            Assert.AreEqual("float", TypeFormatter.Format(typeof(float), @namespace, builtInTypeName));
            Assert.AreEqual("double", TypeFormatter.Format(typeof(double), @namespace, builtInTypeName));
            Assert.AreEqual("decimal", TypeFormatter.Format(typeof(decimal), @namespace, builtInTypeName));
            Assert.AreEqual("string", TypeFormatter.Format(typeof(string), @namespace, builtInTypeName));
            Assert.AreEqual("object", TypeFormatter.Format(typeof(object), @namespace, builtInTypeName));
            Assert.AreEqual("string[]", TypeFormatter.Format(typeof(string[]), @namespace, builtInTypeName));
            Assert.AreEqual("string[,]", TypeFormatter.Format(typeof(string[,]), @namespace, builtInTypeName));
            Assert.AreEqual("System.Collections.Generic.Dictionary<,>", TypeFormatter.Format(typeof(Dictionary<,>), @namespace, builtInTypeName));
            Assert.AreEqual("System.Collections.Generic.Dictionary<int,string>", TypeFormatter.Format(typeof(Dictionary<int, string>), @namespace, builtInTypeName));
            Assert.AreEqual("System.Collections.Generic.Dictionary<,>.Enumerator<,>", TypeFormatter.Format(typeof(Dictionary<,>.Enumerator), @namespace, builtInTypeName));
            Assert.AreEqual("<>f__AnonymousType0", TypeFormatter.Format(anonymousEmpty.GetType(), @namespace, builtInTypeName));
            Assert.AreEqual("<>f__AnonymousType1<int,string>", TypeFormatter.Format(anonymousIntString.GetType(), @namespace, builtInTypeName));

            // not including namespace, not using built in type name
            @namespace = false;
            builtInTypeName = false;
            Assert.AreEqual("Boolean", TypeFormatter.Format(typeof(bool), @namespace, builtInTypeName));
            Assert.AreEqual("Char", TypeFormatter.Format(typeof(char), @namespace, builtInTypeName));
            Assert.AreEqual("SByte", TypeFormatter.Format(typeof(sbyte), @namespace, builtInTypeName));
            Assert.AreEqual("Byte", TypeFormatter.Format(typeof(byte), @namespace, builtInTypeName));
            Assert.AreEqual("Int16", TypeFormatter.Format(typeof(short), @namespace, builtInTypeName));
            Assert.AreEqual("UInt16", TypeFormatter.Format(typeof(ushort), @namespace, builtInTypeName));
            Assert.AreEqual("Int32", TypeFormatter.Format(typeof(int), @namespace, builtInTypeName));
            Assert.AreEqual("UInt32", TypeFormatter.Format(typeof(uint), @namespace, builtInTypeName));
            Assert.AreEqual("Int64", TypeFormatter.Format(typeof(long), @namespace, builtInTypeName));
            Assert.AreEqual("UInt64", TypeFormatter.Format(typeof(ulong), @namespace, builtInTypeName));
            Assert.AreEqual("Single", TypeFormatter.Format(typeof(float), @namespace, builtInTypeName));
            Assert.AreEqual("Double", TypeFormatter.Format(typeof(double), @namespace, builtInTypeName));
            Assert.AreEqual("Decimal", TypeFormatter.Format(typeof(decimal), @namespace, builtInTypeName));
            Assert.AreEqual("String", TypeFormatter.Format(typeof(string), @namespace, builtInTypeName));
            Assert.AreEqual("Object", TypeFormatter.Format(typeof(object), @namespace, builtInTypeName));
            Assert.AreEqual("String[]", TypeFormatter.Format(typeof(string[]), @namespace, builtInTypeName));
            Assert.AreEqual("String[,]", TypeFormatter.Format(typeof(string[,]), @namespace, builtInTypeName));
            Assert.AreEqual("Dictionary<,>", TypeFormatter.Format(typeof(Dictionary<,>), @namespace, builtInTypeName));
            Assert.AreEqual("Dictionary<Int32,String>", TypeFormatter.Format(typeof(Dictionary<int, string>), @namespace, builtInTypeName));
            Assert.AreEqual("Dictionary<,>.Enumerator<,>", TypeFormatter.Format(typeof(Dictionary<,>.Enumerator), @namespace, builtInTypeName));
            Assert.AreEqual("<>f__AnonymousType0", TypeFormatter.Format(anonymousEmpty.GetType(), @namespace, builtInTypeName));
            Assert.AreEqual("<>f__AnonymousType1<Int32,String>", TypeFormatter.Format(anonymousIntString.GetType(), @namespace, builtInTypeName));

            // including namespace, not using built in type name
            @namespace = true;
            builtInTypeName = false;
            Assert.AreEqual("System.Boolean", TypeFormatter.Format(typeof(bool), @namespace, builtInTypeName));
            Assert.AreEqual("System.Char", TypeFormatter.Format(typeof(char), @namespace, builtInTypeName));
            Assert.AreEqual("System.SByte", TypeFormatter.Format(typeof(sbyte), @namespace, builtInTypeName));
            Assert.AreEqual("System.Byte", TypeFormatter.Format(typeof(byte), @namespace, builtInTypeName));
            Assert.AreEqual("System.Int16", TypeFormatter.Format(typeof(short), @namespace, builtInTypeName));
            Assert.AreEqual("System.UInt16", TypeFormatter.Format(typeof(ushort), @namespace, builtInTypeName));
            Assert.AreEqual("System.Int32", TypeFormatter.Format(typeof(int), @namespace, builtInTypeName));
            Assert.AreEqual("System.UInt32", TypeFormatter.Format(typeof(uint), @namespace, builtInTypeName));
            Assert.AreEqual("System.Int64", TypeFormatter.Format(typeof(long), @namespace, builtInTypeName));
            Assert.AreEqual("System.UInt64", TypeFormatter.Format(typeof(ulong), @namespace, builtInTypeName));
            Assert.AreEqual("System.Single", TypeFormatter.Format(typeof(float), @namespace, builtInTypeName));
            Assert.AreEqual("System.Double", TypeFormatter.Format(typeof(double), @namespace, builtInTypeName));
            Assert.AreEqual("System.Decimal", TypeFormatter.Format(typeof(decimal), @namespace, builtInTypeName));
            Assert.AreEqual("System.String", TypeFormatter.Format(typeof(string), @namespace, builtInTypeName));
            Assert.AreEqual("System.Object", TypeFormatter.Format(typeof(object), @namespace, builtInTypeName));
            Assert.AreEqual("System.String[]", TypeFormatter.Format(typeof(string[]), @namespace, builtInTypeName));
            Assert.AreEqual("System.String[,]", TypeFormatter.Format(typeof(string[,]), @namespace, builtInTypeName));
            Assert.AreEqual("System.Collections.Generic.Dictionary<,>", TypeFormatter.Format(typeof(Dictionary<,>), @namespace, builtInTypeName));
            Assert.AreEqual("System.Collections.Generic.Dictionary<System.Int32,System.String>", TypeFormatter.Format(typeof(Dictionary<int, string>), @namespace, builtInTypeName));
            Assert.AreEqual("System.Collections.Generic.Dictionary<,>.Enumerator<,>", TypeFormatter.Format(typeof(Dictionary<,>.Enumerator), @namespace, builtInTypeName));
            Assert.AreEqual("<>f__AnonymousType0", TypeFormatter.Format(anonymousEmpty.GetType(), @namespace, builtInTypeName));
            Assert.AreEqual("<>f__AnonymousType1<System.Int32,System.String>", TypeFormatter.Format(anonymousIntString.GetType(), @namespace, builtInTypeName));
        }
    }
}
