﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using AbbreviationScore;

namespace AbbreviationScoreSample
{
    public partial class MainPage : UserControl
    {
        public MainPage()
        {
            InitializeComponent();

            // sample
            stringsTextBox.Text = "Internet Explorer\n" +
                                  "Paint.NET\n" +
                                  "iTunes\n" +
                                  "KeyPass Password Safe\n" +
                                  "Launchy\n" +
                                  "Microsoft Visual Studio 2008\n" +
                                  "Mozilla Firefox\n" +
                                  "Norton Internet Security\n";
            abbreviationTextBox.Text = "i";
            Compute();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Compute();
        }

        private void Compute()
        {
            var strs = stringsTextBox.Text.Split(new char[] { '\r', '\n', }, StringSplitOptions.RemoveEmptyEntries);
            string abbrev = abbreviationTextBox.Text;
            var abbrevScorer = new AbbreviationScorer();
            var results = new List<KeyValuePair<double, string>>();
            foreach (var str in strs)
            {
                double score = abbrevScorer.Compute(str, abbrev);
                results.Add(new KeyValuePair<double, string>(score, str));
            }
            results.Sort((x, y) => y.Key.CompareTo(x.Key));

            var builder = new StringBuilder();
            results.ForEach(x => builder.AppendFormat("{0:F3}\t{1:G}\n", x.Key, x.Value));
            resultsTextBox.Text = builder.ToString();
        }

    }
}
