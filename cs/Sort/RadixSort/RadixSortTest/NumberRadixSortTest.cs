﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RadixSort;

namespace RadixSortTest
{
    [TestClass]
    public class NumberRadixSortTest
    {
        public NumberRadixSortTest() { }

        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        public const int RANDOM_TEST_COUNT = 32;


        [TestMethod]
        public void TestSortUInt32()
        {
            var list = new UInt32[] { 0, 1, 2, 
                                      unchecked((UInt32)(-1)), unchecked((UInt32)(-2)), 
                                      UInt32.MinValue, UInt32.MaxValue, 
                                      0x01234500, 0x01230067, 0x01004567, 0x00234567, };
            // ascending
            {
                var expected = (UInt32[])list.Clone();
                Array.Sort(expected);
                var result = (UInt32[])list.Clone();
                NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Ascending);
                CollectionAssert.AreEqual(expected, result);
            }
            // descending
            {
                var expected = (UInt32[])list.Clone();
                Array.Sort(expected, (x, y) => x < y ? 1 : x > y ? -1 : 0);
                var result = (UInt32[])list.Clone();
                NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Descending);
                CollectionAssert.AreEqual(expected, result);
            }
        }

        [TestMethod]
        public void TestSortUInt64()
        {
            var list = new UInt64[] { 0, 1, 2, 
                                      unchecked((UInt64)(-1)), unchecked((UInt64)(-2)), 
                                      UInt64.MinValue, UInt64.MaxValue, 
                                      0x0123456789ABCD00, 0x0123456789AB00EF, 0x012345678900CDEF, 0x0123456700ABCDEF, 
                                      0x0123450089ABCDEF, 0x0123006789ABCDEF, 0x0100456789ABCDEF, 0x0023456789ABCDEF, };
            // ascending
            {
                var expected = (UInt64[])list.Clone();
                Array.Sort(expected);
                var result = (UInt64[])list.Clone();
                NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Ascending);
                CollectionAssert.AreEqual(expected, result);
            }
            // descending
            {
                var expected = (UInt64[])list.Clone();
                Array.Sort(expected, (x, y) => x < y ? 1 : x > y ? -1 : 0);
                var result = (UInt64[])list.Clone();
                NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Descending);
                CollectionAssert.AreEqual(expected, result);
            }
        }

        [TestMethod]
        public void TestSortInt32()
        {
            var list = new Int32[] { 0, 1, 2, 
                                     -1, -2, 
                                     Int32.MinValue, Int32.MaxValue, 
                                     0x01234500, 0x01230067, 0x01004567, 0x00234567, };
            // ascending
            {
                var expected = (Int32[])list.Clone();
                Array.Sort(expected);
                var result = (Int32[])list.Clone();
                NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Ascending);
                CollectionAssert.AreEqual(expected, result);
            }
            // descending
            {
                var expected = (Int32[])list.Clone();
                Array.Sort(expected, (x, y) => x < y ? 1 : x > y ? -1 : 0);
                var result = (Int32[])list.Clone();
                NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Descending);
                CollectionAssert.AreEqual(expected, result);
            }
        }

        [TestMethod]
        public void TestSortInt64()
        {
            var list = new Int64[] { 0, 1, 2, 
                                     -1, -2, 
                                     Int64.MinValue, Int64.MaxValue, 
                                     0x0123456789ABCD00, 0x0123456789AB00EF, 0x012345678900CDEF, 0x0123456700ABCDEF, 
                                     0x0123450089ABCDEF, 0x0123006789ABCDEF, 0x0100456789ABCDEF, 0x0023456789ABCDEF, };
            // ascending
            {
                var expected = (Int64[])list.Clone();
                Array.Sort(expected);
                var result = (Int64[])list.Clone();
                NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Ascending);
                CollectionAssert.AreEqual(expected, result);
            }
            // descending
            {
                var expected = (Int64[])list.Clone();
                Array.Sort(expected, (x, y) => x < y ? 1 : x > y ? -1 : 0);
                var result = (Int64[])list.Clone();
                NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Descending);
                CollectionAssert.AreEqual(expected, result);
            }
        }

        [TestMethod]
        unsafe public void TestSortSingle()
        {
            Func<Int32, Single> toSingle = x => *(Single*)&x;
            var list = new Single[] { 0.0F, -0.0F, 0.5F, 1.0F, 1.5F, -0.5F, -1.0F, -1.5F, 
                                      Single.MinValue, Single.MaxValue, Single.Epsilon, Single.NegativeInfinity, Single.PositiveInfinity, 
                                      toSingle(0x01234500), toSingle(0x01230067), toSingle(0x01004567), toSingle(0x00234567), };
            // ascending
            {
                var expected = (Single[])list.Clone();
                Array.Sort(expected);
                var result = (Single[])list.Clone();
                NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Ascending);
                CollectionAssert.AreEqual(expected, result);
            }
            // descending
            {
                var expected = (Single[])list.Clone();
                Array.Sort(expected, (x, y) => x < y ? 1 : x > y ? -1 : 0);
                var result = (Single[])list.Clone();
                NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Descending);
                CollectionAssert.AreEqual(expected, result);
            }
        }

        [TestMethod]
        public void TestSortDouble()
        {
            Func<Int64, Double> toDouble = x => BitConverter.Int64BitsToDouble(x);
            var list = new Double[] { 0.0D, -0.0D, 0.5D, 1.0D, 1.5D, -0.5D, -1.0D, -1.5D, 
                                      Double.MinValue, Double.MaxValue, Double.Epsilon, Double.NegativeInfinity, Double.PositiveInfinity, 
                                      toDouble(0x0123456789ABCD00), toDouble(0x0123456789AB00EF), toDouble(0x012345678900CDEF), toDouble(0x0123456700ABCDEF), 
                                      toDouble(0x0123450089ABCDEF), toDouble(0x0123006789ABCDEF), toDouble(0x0100456789ABCDEF), toDouble(0x0023456789ABCDEF), };
            // ascending
            {
                var expected = (Double[])list.Clone();
                Array.Sort(expected);
                var result = (Double[])list.Clone();
                NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Ascending);
                CollectionAssert.AreEqual(expected, result);
            }
            // descending
            {
                var expected = (Double[])list.Clone();
                Array.Sort(expected, (x, y) => x < y ? 1 : x > y ? -1 : 0);
                var result = (Double[])list.Clone();
                NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Descending);
                CollectionAssert.AreEqual(expected, result);
            }
        }


        [TestMethod]
        public void TestSortUInt32Random()
        {
            for (int count = 0; count < RANDOM_TEST_COUNT; count++)
            {
                var random = new Random(count);
                var list = Enumerable.Range(0, random.Next(UInt16.MaxValue))
                                     .Select(_ => (UInt32)random.Next(Int32.MinValue, Int32.MaxValue))
                                     .ToArray();
                // ascending
                {
                    var expected = (UInt32[])list.Clone();
                    Array.Sort(expected);
                    var result = (UInt32[])list.Clone();
                    NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Ascending);
                    CollectionAssert.AreEqual(expected, result);
                }
                // descending
                {
                    var expected = (UInt32[])list.Clone();
                    Array.Sort(expected, (x, y) => x < y ? 1 : x > y ? -1 : 0);
                    var result = (UInt32[])list.Clone();
                    NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Descending);
                    CollectionAssert.AreEqual(expected, result);
                }
            }
        }

        [TestMethod]
        public void TestSortUInt64Random()
        {
            for (int count = 0; count < RANDOM_TEST_COUNT; count++)
            {
                var random = new Random(count);
                var list = Enumerable.Range(0, random.Next(UInt16.MaxValue))
                                     .Select(_ => (UInt64)((random.NextDouble() - 0.5) * 2.0 * Int64.MinValue))
                                     .ToArray();
                // ascending
                {
                    var expected = (UInt64[])list.Clone();
                    Array.Sort(expected);
                    var result = (UInt64[])list.Clone();
                    NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Ascending);
                    CollectionAssert.AreEqual(expected, result);
                }
                // descending
                {
                    var expected = (UInt64[])list.Clone();
                    Array.Sort(expected, (x, y) => x < y ? 1 : x > y ? -1 : 0);
                    var result = (UInt64[])list.Clone();
                    NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Descending);
                    CollectionAssert.AreEqual(expected, result);
                }
            }
        }

        [TestMethod]
        public void TestSortInt32Random()
        {
            for (int count = 0; count < RANDOM_TEST_COUNT; count++)
            {
                var random = new Random(count);
                var list = Enumerable.Range(0, random.Next(UInt16.MaxValue))
                                     .Select(_ => random.Next(Int32.MinValue, Int32.MaxValue))
                                     .ToArray();
                // ascending
                {
                    var expected = (Int32[])list.Clone();
                    Array.Sort(expected);
                    var result = (Int32[])list.Clone();
                    NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Ascending);
                    CollectionAssert.AreEqual(expected, result);
                }
                // descending
                {
                    var expected = (Int32[])list.Clone();
                    Array.Sort(expected, (x, y) => x < y ? 1 : x > y ? -1 : 0);
                    var result = (Int32[])list.Clone();
                    NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Descending);
                    CollectionAssert.AreEqual(expected, result);
                }
            }
        }

        [TestMethod]
        public void TestSortInt64Random()
        {
            for (int count = 0; count < RANDOM_TEST_COUNT; count++)
            {
                var random = new Random(count);
                var list = Enumerable.Range(0, random.Next(UInt16.MaxValue))
                                     .Select(_ => (Int64)((random.NextDouble() - 0.5) * 2.0 * Int64.MinValue))
                                     .ToArray();
                // ascending
                {
                    var expected = (Int64[])list.Clone();
                    Array.Sort(expected);
                    var result = (Int64[])list.Clone();
                    NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Ascending);
                    CollectionAssert.AreEqual(expected, result);
                }
                // descending
                {
                    var expected = (Int64[])list.Clone();
                    Array.Sort(expected, (x, y) => x < y ? 1 : x > y ? -1 : 0);
                    var result = (Int64[])list.Clone();
                    NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Descending);
                    CollectionAssert.AreEqual(expected, result);
                }
            }
        }

        [TestMethod]
        public void TestSortSingleRandom()
        {
            for (int count = 0; count < RANDOM_TEST_COUNT; count++)
            {
                var random = new Random(count);
                var list = Enumerable.Range(0, random.Next(UInt16.MaxValue))
                                     .Select(_ => (Single)((random.NextDouble() - 0.5) * 2.0))
                                     .ToArray();
                // ascending
                {
                    var expected = (Single[])list.Clone();
                    Array.Sort(expected);
                    var result = (Single[])list.Clone();
                    NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Ascending);
                    CollectionAssert.AreEqual(expected, result);
                }
                // descending
                {
                    var expected = (Single[])list.Clone();
                    Array.Sort(expected, (x, y) => x < y ? 1 : x > y ? -1 : 0);
                    var result = (Single[])list.Clone();
                    NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Descending);
                    CollectionAssert.AreEqual(expected, result);
                }
            }
        }

        [TestMethod]
        public void TestSortDoubleRandom()
        {
            for (int count = 0; count < RANDOM_TEST_COUNT; count++)
            {
                var random = new Random(count);
                var list = Enumerable.Range(0, random.Next(UInt16.MaxValue))
                                     .Select(_ => (random.NextDouble() - 0.5) * 2.0)
                                     .ToArray();
                // ascending
                {
                    var expected = (Double[])list.Clone();
                    Array.Sort(expected);
                    var result = (Double[])list.Clone();
                    NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Ascending);
                    CollectionAssert.AreEqual(expected, result);
                }
                // descending
                {
                    var expected = (Double[])list.Clone();
                    Array.Sort(expected, (x, y) => x < y ? 1 : x > y ? -1 : 0);
                    var result = (Double[])list.Clone();
                    NumberRadixSorter.Sort(result, NumberRadixSorter.SortOrder.Descending);
                    CollectionAssert.AreEqual(expected, result);
                }
            }
        }
    }
}
