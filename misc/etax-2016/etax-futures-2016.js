// 2016年分（平成28年分）「【確定申告書作成コーナー】-先物取引に係る雑所得等」入力スクリプト
// 【確定申告書作成コーナー】-先物取引に係る雑所得等 https://www.keisan.nta.go.jp/h28/syotoku/TaMapSelect?p_send=ta_subB62.jsp
// usage: cscript.exe etax-futures-2016.js

// WSH/JScriptで配列の便利メソッドライブラリの案・compact/include/uniqなどを追加 - ソフトウェア勉強ログとサンプルコード http://source-code-student.hatenablog.jp/entry/20150121/p1
Array.from = function (iterable, getLength, getItem) {
    var array = [];
    var length = getLength ? getLength(iterable) : iterable.length;
    for (var n = 0; n < length; n++) array.push(getItem ? getItem(iterable, n) : iterable[n]);
    return array;
};
Array.prototype.each = function (func) {
    for (var n = 0; n < this.length; n++) func.call(this, this[n], n);
    return this;
};
Array.prototype.reduce = function (func, initial) {
    var result = initial;
    this.each(function (item, index) { result = func(result, item, index); });
    return result;
};
Array.prototype.filter = function (func) {
    var result = this.reduce(
		function (result, item) {
		    if (func(item)) result.push(item);
		    return result;
		},
		[]
	);
    return result;
};

// WSH:JScript(javascript)でヒアドキュメント - じゅんじゅんのきまぐれ http://d.hatena.ne.jp/junjun777/20111004/javascript_here_document
var _source;
function heredoc(name) {
    if (!_source) {
        var fso = WScript.CreateObject("Scripting.FileSystemObject");
        var file = fso.OpenTextFile(WScript.ScriptFullName);
        _source = file.ReadAll();
        file.Close();
    }
    var re = new RegExp("^\\s*/\\*\\s*" + name + "[ \\f\\r\\t\\v]*\\n((.|\\s)*?)(?=\\n[ \\f\\r\\t\\v]*\\*/)", "m");
    var result;
    if (re.test(_source))
        result = RegExp.$1;
    return result;
};

// 
(function () {
    var IEXPLORER_PATH = "C:\\Program Files (x86)\\Internet Explorer\\IEXPLORE.EXE";
    var FUTURES_PAGE_URL = "https://www.keisan.nta.go.jp/h28/syotoku/TaMapSelect?p_send=ta_subB62.jsp#bbctrl";

    var windows;  // reusing Internet Explorer COM Automation Object - Stack Overflow http://stackoverflow.com/questions/941767/reusing-internet-explorer-com-automation-object
    windows = Array.from(
        WScript.CreateObject("Shell.Application").Windows(),
        function (iterable) { return iterable.Count; },
        function (iterable, index) { return iterable.Item(index); });
    windows = windows.filter(
        function (w) { return w && w.FullName == IEXPLORER_PATH && w.LocationURL == FUTURES_PAGE_URL; });
    var document = windows[0].document;

    var anchors = Array.from(document.getElementsByTagName("a"));
    var addNewAnchor = anchors.filter(function (a) { return a.title == "もう１件入力する"; })[0];

    var setValue = function (name, value, event) {
        var element = document.getElementsByName(name)[0];
        element.value = value;
        if (event)
            element.fireEvent(event);
    };

    //setValue("s62_Flg", "2", "");  // 所得区分 1: 事業所得用, 3: 譲渡所得用, 2: 雑所得用
    var futuresText = heredoc("futures");
    var futuresLines= futuresText.split("\n");
    futuresLines.each(function (line) {
        var values = line.split("\t");

        // 取引の内容
        setValue("s62_1", values[0], "onBlur");  // 種類
        setValue("s62_kmm", values[1], "onchange");  // 決済年月日
        setValue("s62_kdd", values[2], "onchange");  // 決済年月日
        setValue("s62_3", values[3], "onBlur");  // 数量
        setValue("s62_4", values[4], "");  // 決済の方法

        // 収入
        setValue("s62_5", values[5], "onBlur");  // 差金等決済に係る利益又は損失の額
        setValue("s62_21", values[6], "onBlur");  // 譲渡による収入金額
        setValue("s62_6", values[7], "onBlur");  // その他の収入
        // 必要経費等
        setValue("s62_7", values[8], "onBlur");  // 委託手数料
        setValue("s62_22", values[9], "onBlur");  // 譲渡による収入金額に係る取得費
        setValue("s62_8_0", values[10], "");  // s62_8_0
        setValue("s62_8", values[11], "onBlur");  // s62_8
        setValue("s62_9_0", values[12], "");  // s62_9_0
        setValue("s62_9", values[13], "onBlur");  // s62_9
        setValue("s62_10_0", values[14], "");  // s62_10_0
        setValue("s62_10", values[15], "onBlur");  // s62_10

        addNewAnchor.fireEvent("onclick");
        while (document.readyState != "complete")
            WScript.Sleep(100);
    });
})();
WScript.Quit();

// 種類	決済月	決済日	数量	決済の方法	差金等決済に係る利益又は損失の額	譲渡による収入金額	その他の収入	委託手数料	譲渡による収入金額に係る取得費	必要経費3項目名	必要経費3	必要経費4項目名	必要経費4	必要経費5項目	必要経費5
/* futures
日経平均先物ミニ	2	4	1	先物返済	10500			74		消費税	4				
日経平均先物ミニ	4	15	1	先物返済	-1000			74		消費税	4				
日経平均先物ミニ	6	10	1	先物最終決済	65911			74		消費税	4				
日経平均先物ミニ	6	10	1	先物最終決済	-66911			74		消費税	4				
日経平均先物ミニ	11	9	1	先物返済	4000			74		消費税	4				
*/
