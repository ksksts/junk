// ==UserScript==
// @name Newsweekjapan HTML link
// @namespace http://ksksts.blogspot.com/
// @description Changes newsweekjapan.jp to use HTML link instead of javascript function.
// @include http://newsweekjapan.jp/
// @include http://newsweekjapan.jp/*/
// @include http://newsweekjapan.jp/special/*
// ==/UserScript==

(function() {
    var entries = $X('//div[(contains(@class, "entry") or contains(@class, "showCaseBody")) and @onclick and @onkeydown]');
    for (var n = 0; n < entries.length; n++) {
        var entry = entries[n];
        if (!entry.getAttribute('onclick').match(/clickLink\('([^']+)', ''\);/))
            continue;
        var url = RegExp.$1;
        var elems = $X('.//h3|.//p|.//h4|.//h5', entry);
        for (var m = 0; m < elems.length; m++) {
            var elem = elems[m];
            var anchor = $N('a', {'href': url});
            while (elem.firstChild)
                anchor.appendChild(elem.removeChild(elem.firstChild));
            elem.appendChild(anchor);
        }
        var img = $X('.//div[contains(@class, "entryImg") or contains(@class, "showCaseImg")]//img', entry)[0];
        if (img) {
            var nimg = $N('a', {'href': url}, [img.cloneNode(true)]);
            img.parentNode.replaceChild(nimg, img);
        }
        
        entry.setAttribute('class', entry.getAttribute('class').replace('clickable', ''));
        entry.removeAttribute('onclick');
        entry.removeAttribute('onkeydown');
    }
    
    GM_addStyle(<><![CDATA[
        div.entry h3 a/*, div.entry p a*/, div.entry h5 a {
            text-decoration: none;
        }
        div.entry h3 a:hover, div.entry p a:hover, div.entry h5 a:hover, 
        div.entry h3 a:focus, div.entry p a:focus, div.entry h5 a:focus, 
        div.entry h3 a:active, div.entry p a:active, div.entry h5 a:active {
            text-decoration: underline;
            color: #BF1B02;
        }
        div.showCaseBody h3 a, div.showCaseBody h4 a {
            text-decoration: none;
        }
        div.showCaseBody h3 a:hover, div.showCaseBody h4 a:hover, 
        div.showCaseBody h3 a:focus, div.showCaseBody h4 a:focus, 
        div.showCaseBody h3 a:active, div.showCaseBody h4 a:active {
            text-decoration: underline;
            color: #BF1B02;
        }
    ]]></>);
    
    
    /// http://lowreal.net/blog/2007/11/17/1
    // $X(exp);
    // $X(exp, context);
    // $X(exp, type);
    // $X(exp, context, type);
    function $X (exp, context, type /* want type */) {
        if (typeof context == "function") {
            type    = context;
            context = null;
        }
        if (!context) context = document;
        var exp = (context.ownerDocument || context).createExpression(exp, function (prefix) {
            var o = document.createNSResolver(context).lookupNamespaceURI(prefix);
            if (o) return o;
            return (document.contentType == "application/xhtml+xml") ? "http://www.w3.org/1999/xhtml" : "";
        });

        switch (type) {
            case String:
                return exp.evaluate(
                    context,
                    XPathResult.STRING_TYPE,
                    null
                ).stringValue;
            case Number:
                return exp.evaluate(
                    context,
                    XPathResult.NUMBER_TYPE,
                    null
                ).numberValue;
            case Boolean:
                return exp.evaluate(
                    context,
                    XPathResult.BOOLEAN_TYPE,
                    null
                ).booleanValue;
            case Array:
                var result = exp.evaluate(
                    context,
                    XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
                    null
                );
                var ret = [];
                for (var i = 0, len = result.snapshotLength; i < len; i++) {
                    ret.push(result.snapshotItem(i));
                }
                return ret;
            case undefined:
                var result = exp.evaluate(context, XPathResult.ANY_TYPE, null);
                switch (result.resultType) {
                    case XPathResult.STRING_TYPE : return result.stringValue;
                    case XPathResult.NUMBER_TYPE : return result.numberValue;
                    case XPathResult.BOOLEAN_TYPE: return result.booleanValue;
                    case XPathResult.UNORDERED_NODE_ITERATOR_TYPE: {
                        // not ensure the order.
                        var ret = [];
                        var i = null;
                        while (i = result.iterateNext()) {
                            ret.push(i);
                        }
                        return ret;
                    }
                }
                return null;
            default:
                throw(TypeError("$X: specified type is not valid type."));
        }
    }
    
    /// http://www.amazon.co.jp/dp/4844323644/
    function $N(name, attr, childs) {
        var ret = document.createElement(name);
        for (k in attr) {
            if (!attr.hasOwnProperty(k)) continue;
            v = attr[k];
            if (k == "class") {
                ret.className = v;
            } else {
                ret.setAttribute(k, v);
            }
        }
        switch (typeof childs) {
            case "string": {
                ret.appendChild(document.createTextNode(childs));
                break;
            }
            case "object": {
                for (var i = 0, len = childs.length; i < len; i++) {
                    var child = childs[i];
                    if (typeof child == "string") {
                        ret.appendChild(document.createTextNode(child));
                    } else {
                        ret.appendChild(child);
                    }
                }
                break;
            }
        }
        return ret;
    }
})();
