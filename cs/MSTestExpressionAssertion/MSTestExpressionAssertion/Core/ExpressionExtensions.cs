﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace MSTestExpressionAssertion.Core
{
    internal static class ExpressionExtensions
    {
        public static bool IsThis(this ConstantExpression expression)
        {
            var value = expression.Value;
            if (value == null || value.GetType().Name.StartsWith("<"))
                return false;
            if (expression.ToString() != string.Format("value({0})", value.ToString()))
                return false;
            return true;
        }

        public static bool IsLocal(this ConstantExpression expression)
        {
            return expression.Type.Name.StartsWith("<");
        }

        public static bool IsThisMethodCall(this MethodCallExpression expression)
        {
            var expr = expression.Object as ConstantExpression;
            if (expr == null)
                return false;
            return IsThis(expr);
        }

        public static bool IsThisMember(this MemberExpression expression)
        {
            var expr = expression.Expression as ConstantExpression;
            if (expr == null)
                return false;
            return IsThis(expr);
        }

        public static bool IsLocalMember(this MemberExpression expression)
        {
            var expr = expression.Expression as ConstantExpression;
            if (expr == null)
                return false;
            return IsLocal(expr);
        }

        public static bool IsIndexer(this MethodCallExpression expression)
        {
            return expression.Method.Name == "get_Item";
        }
    }
}
