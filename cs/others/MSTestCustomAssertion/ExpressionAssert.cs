﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MSTestCustomAssertion
{
    public class ExpressionAssert
    {
        public static void IsTrue(Expression<Func<bool>> expr)
        {
            var result = expr.Compile().Invoke();
            if (!result)
                throw new FailedException("ExpressionAssert.IsTrue failed");
        }

        public class FailedException : AssertFailedException
        {
            public FailedException() : base() { }
            public FailedException(string msg) : base(msg) { }
            public override string StackTrace
            {
                get
                {
                    var lines = base.StackTrace.Split(new string[] { Environment.NewLine, }, StringSplitOptions.None);
                    var index = Array.FindIndex(lines, x => !x.Contains("ExpressionAssert."));
                    var st = string.Join(Environment.NewLine, lines, index, lines.Length - index);
                    return st;
                }
            }
        }
    }
}
