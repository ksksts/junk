﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using MiniTwitter.Extensions;

namespace MiniTwitter
{
    /// <summary>
    /// TimelineDialog.xaml の相互作用ロジック
    /// </summary>
    public partial class TimelineDialog : Window
    {
        public TimelineDialog()
        {
            InitializeComponent();
        }

        private ObservableCollection<Filter> filters;

        public Timeline Timeline { get; set; }

        private void TimelineDialog_Loaded(object sender, RoutedEventArgs e)
        {
            if (Timeline == null)
            {
                Timeline = new Timeline { Type = TimelineType.User };
            }
            filters = new ObservableCollection<Filter>(Timeline.Filters);
            FilterListView.ItemsSource = filters;
            filters.BeginEdit();
            DataContext = Timeline;
            BindingGroup.BeginEdit();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            if (!BindingGroup.CommitEdit())
            {
                return;
            }
            Timeline.Filters.Clear();
            Timeline.Filters.AddRange(filters);
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            filters.CancelEdit();
            BindingGroup.CancelEdit();
            DialogResult = false;
        }

        private void AddFilterButton_Click(object sender, RoutedEventArgs e)
        {
            var filter = new Filter { Pattern = FilterTextBox.Text, Type = FilterType.None };
            filters.Add(filter);
            filter.BeginEdit();
            FilterListView.ScrollIntoView(filter);
            FilterTextBox.Clear();
        }

        private void DeleteFilterButton_Click(object sender, RoutedEventArgs e)
        {
            var item = (Filter)FilterListView.SelectedItem;

            if (item == null)
            {
                return;
            }

            filters.Remove(item);
        }
    }
}
