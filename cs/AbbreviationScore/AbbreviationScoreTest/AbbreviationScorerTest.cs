﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AbbreviationScore;

namespace AbbreviationScoreTest
{
    [TestClass]
    public class AbbreviationScorerTest
    {
        #region
        public AbbreviationScorerTest() { }

        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }
        #endregion

        #region Test methods
        [TestMethod]
        public void ComputeTest()
        {
            var abbrevScorer = new AbbreviationScorer();
            double delta = 1.0e-3;
            Assert.AreEqual(0.95, abbrevScorer.Compute("FooBar", "foo"), delta);
            Assert.AreEqual(0.917, abbrevScorer.Compute("FooBar", "fb"), delta);
            Assert.AreEqual(0.929, abbrevScorer.Compute("Foo Bar", "fb"), delta);
            Assert.AreEqual(0.0, abbrevScorer.Compute("Foo Bar", "baz"), delta);
            Assert.AreEqual(0.8, abbrevScorer.Compute("Foo Bar", ""), delta);
        }

        [TestMethod]
        public void ComputeScoreArrayTest()
        {
            var abbrevScorer = new AbbreviationScorer_Accessor();
            double n = abbrevScorer.ScoreNoMatch;
            double m = abbrevScorer.ScoreMatch;
            double t = abbrevScorer.ScoreTrailing;
            double s = abbrevScorer.ScoreTrailingButStarted;
            double b = abbrevScorer.ScoreBuffer;
            var comparer = new TolerantComparer(1.0e-3);

            CollectionAssert.AreEqual(new double[] { t, }, BuildScoreArray(abbrevScorer, "", ""), comparer);
            CollectionAssert.AreEqual(new double[] { n, }, BuildScoreArray(abbrevScorer, "", "a"), comparer);
            CollectionAssert.AreEqual(new double[] { t, }, BuildScoreArray(abbrevScorer, "a", ""), comparer);
            CollectionAssert.AreEqual(new double[] { n, }, BuildScoreArray(abbrevScorer, "a", "toolong"), comparer);
            CollectionAssert.AreEqual(new double[] { m, }, BuildScoreArray(abbrevScorer, "a", "a"), comparer);
            CollectionAssert.AreEqual(new double[] { n, }, BuildScoreArray(abbrevScorer, "a", "b"), comparer);
            CollectionAssert.AreEqual(new double[] { t, t, t, }, BuildScoreArray(abbrevScorer, "abc", ""), comparer);
            CollectionAssert.AreEqual(new double[] { m, s, s, }, BuildScoreArray(abbrevScorer, "abc", "a"), comparer);
            CollectionAssert.AreEqual(new double[] { n, m, t, }, BuildScoreArray(abbrevScorer, "abc", "b"), comparer);
            CollectionAssert.AreEqual(new double[] { n, n, m, }, BuildScoreArray(abbrevScorer, "abc", "c"), comparer);
            CollectionAssert.AreEqual(new double[] { n, n, n, }, BuildScoreArray(abbrevScorer, "abc", "d"), comparer);
            CollectionAssert.AreEqual(new double[] { m, }, BuildScoreArray(abbrevScorer, "A", "a"), comparer);
            CollectionAssert.AreEqual(new double[] { n, }, BuildScoreArray(abbrevScorer, "A", "b"), comparer);
            CollectionAssert.AreEqual(new double[] { t, t, t, t, t, t, }, BuildScoreArray(abbrevScorer, "FooBar", ""), comparer);
            CollectionAssert.AreEqual(new double[] { m, m, m, s, s, s, }, BuildScoreArray(abbrevScorer, "FooBar", "foo"), comparer);
            CollectionAssert.AreEqual(new double[] { m, b, b, m, s, s, }, BuildScoreArray(abbrevScorer, "FooBar", "fb"), comparer);
            CollectionAssert.AreEqual(new double[] { b, b, b, m, t, t, }, BuildScoreArray(abbrevScorer, "FooBar", "b"), comparer);
            CollectionAssert.AreEqual(new double[] { n, m, m, n, m, m, }, BuildScoreArray(abbrevScorer, "FooBar", "ooar"), comparer);
            CollectionAssert.AreEqual(new double[] { n, n, n, n, n, n, }, BuildScoreArray(abbrevScorer, "FooBar", "bab"), comparer);
            CollectionAssert.AreEqual(new double[] { t, t, t, t, t, t, t, }, BuildScoreArray(abbrevScorer, "Foo Bar", ""), comparer);
            CollectionAssert.AreEqual(new double[] { m, m, m, s, s, s, s, }, BuildScoreArray(abbrevScorer, "Foo Bar", "foo"), comparer);
            CollectionAssert.AreEqual(new double[] { m, b, b, m, m, s, s, }, BuildScoreArray(abbrevScorer, "Foo Bar", "fb"), comparer);
            CollectionAssert.AreEqual(new double[] { b, b, b, m, m, t, t, }, BuildScoreArray(abbrevScorer, "Foo Bar", "b"), comparer);
            CollectionAssert.AreEqual(new double[] { n, m, m, n, n, m, m, }, BuildScoreArray(abbrevScorer, "Foo Bar", "ooar"), comparer);
            CollectionAssert.AreEqual(new double[] { n, n, n, n, n, n, n, }, BuildScoreArray(abbrevScorer, "Foo Bar", "bab"), comparer);
        }
        #endregion // Test methods

        #region Static methods
        static private double[] BuildScoreArray(AbbreviationScorer_Accessor abbreviationScorer, 
                                                string @string, string abbreviation)
        {
            if (abbreviation.Length == 0) return Enumerable.Repeat<double>(abbreviationScorer.ScoreTrailing, Math.Max(1, @string.Length)).ToArray<double>();
            if (abbreviation.Length > @string.Length) return new double[] { abbreviationScorer.ScoreNoMatch, };
            return abbreviationScorer.BuildScoreArray(@string, abbreviation);
        }
        #endregion // Static methods

        #region Inner classes
        private class TolerantComparer : IComparer
        {
            public double Delta { get; set; }
            public TolerantComparer(double delta) { Delta = delta; }
            public int Compare(object x, object y)
            {
                double? dx = x as double?;
                double? dy = y as double?;
                if (dx == null || dy == null) return -1;
                return dx < dy - Delta ? -1 : dx > dy + Delta ? 1 : 0;
            }
        }
        #endregion // Inner classes
    }
}
