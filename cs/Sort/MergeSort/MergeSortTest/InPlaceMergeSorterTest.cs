﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MergeSort;

namespace MergeSortTest
{
    [TestClass]
    public class InPlaceMergeSorterTest
    {
        struct Pair<TFirst, TSecond>
        {
            public TFirst First { get; set; }
            public TSecond Second { get; set; }
            public Pair(TFirst first, TSecond second)
                : this()
            {
                First = first;
                Second = second;
            }
            public override string ToString()
            {
                return string.Format("{0}, {1}", First, Second);
            }
        }


        public InPlaceMergeSorterTest() { }

        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        public const int RANDOM_TEST_COUNT = 32;

        [TestMethod]
        public void TestSortRandom()
        {
            for (int count = 0; count < RANDOM_TEST_COUNT; count++)
            {
                var random = new Random(count);
                var size = random.Next(UInt16.MaxValue);
                var list = Enumerable.Range(0, size)
                                     .Select(x => new Pair<Int32, Int32>(random.Next(Int32.MinValue, Int32.MaxValue) % (size / 2), x))
                                     .ToArray();
                MergeSorter.Sort(list, (x, y) => x.First.CompareTo(y.First));
                //list = list.OrderBy(x => x.First).ToArray();
                for (var n = 1; n < list.Length; n++)
                {
                    Assert.IsFalse(list[n - 1].First > list[n].First);
                    if (list[n - 1].First == list[n].First)
                        Assert.IsTrue(list[n - 1].Second < list[n].Second);
                }
            }
        }
    }
}
