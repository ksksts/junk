﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NumberBytes
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine(string.Format("BitConverter.IsLittleEndian: {0}", BitConverter.IsLittleEndian));
            WriteLine("");

            PrintUInt32();
            PrintUInt64();
            PrintInt32();
            PrintInt64();
            PrintSingle();
            PrintDouble();
            PrintDecimal();
        }

        unsafe private static void PrintUInt32()
        {
            Func<UInt32, byte[]> toBytes = x =>
            {
                var p = (byte*)&x;
                var bytes = new byte[sizeof(UInt32)];
                for (var n = 0; n < bytes.Length; n++)
                    bytes[n] = *p++;
                return bytes;
            };
            WriteLine("UInt32:");
            WriteLine("0U", toBytes(0U));
            WriteLine("1U", toBytes(1U));
            WriteLine("2U", toBytes(2U));
            WriteLine("unchecked((UInt32)(-1))", toBytes(unchecked((UInt32)(-1))));
            WriteLine("unchecked((UInt32)(-2))", toBytes(unchecked((UInt32)(-2))));
            WriteLine("UInt32.MinValue", toBytes(UInt32.MinValue));
            WriteLine("UInt32.MaxValue", toBytes(UInt32.MaxValue));
            WriteLine("");
        }

        unsafe private static void PrintUInt64()
        {
            Func<UInt64, byte[]> toBytes = x =>
            {
                var p = (byte*)&x;
                var bytes = new byte[sizeof(UInt64)];
                for (var n = 0; n < bytes.Length; n++)
                    bytes[n] = *p++;
                return bytes;
            };
            WriteLine("UInt64:");
            WriteLine("0UL", toBytes(0UL));
            WriteLine("1UL", toBytes(1UL));
            WriteLine("2UL", toBytes(2UL));
            WriteLine("unchecked((UInt32)(-1))", toBytes(unchecked((UInt64)(-1))));
            WriteLine("unchecked((UInt32)(-2))", toBytes(unchecked((UInt64)(-2))));
            WriteLine("UInt64.MinValue", toBytes(UInt64.MinValue));
            WriteLine("UInt64.MaxValue", toBytes(UInt64.MaxValue));
            WriteLine("");
        }

        unsafe private static void PrintInt32()
        {
            Func<Int32, byte[]> toBytes = x =>
            {
                var p = (byte*)&x;
                var bytes = new byte[sizeof(Int32)];
                for (var n = 0; n < bytes.Length; n++)
                    bytes[n] = *p++;
                return bytes;
            };
            WriteLine("Int32:");
            WriteLine("0", toBytes(0));
            WriteLine("1", toBytes(1));
            WriteLine("2", toBytes(2));
            WriteLine("-1", toBytes(-1));
            WriteLine("-2", toBytes(-2));
            WriteLine("Int32.MinValue", toBytes(Int32.MinValue));
            WriteLine("Int32.MaxValue", toBytes(Int32.MaxValue));
            WriteLine("");
        }

        unsafe private static void PrintInt64()
        {
            Func<Int64, byte[]> toBytes = x =>
            {
                var p = (byte*)&x;
                var bytes = new byte[sizeof(Int64)];
                for (var n = 0; n < bytes.Length; n++)
                    bytes[n] = *p++;
                return bytes;
            };
            WriteLine("Int64:");
            WriteLine("0", toBytes(0L));
            WriteLine("1L", toBytes(1L));
            WriteLine("2L", toBytes(2L));
            WriteLine("-1L", toBytes(-1L));
            WriteLine("-2L", toBytes(-2L));
            WriteLine("Int64.MinValue", toBytes(Int64.MinValue));
            WriteLine("Int64.MaxValue", toBytes(Int64.MaxValue));
            WriteLine("");
        }

        unsafe private static void PrintSingle()
        {
            Func<Single, byte[]> toBytes = x =>
            {
                var p = (byte*)&x;
                var bytes = new byte[sizeof(Single)];
                for (var n = 0; n < bytes.Length; n++)
                    bytes[n] = *p++;
                return bytes;
            };
            WriteLine("Single:");
            WriteLine("0.0F", toBytes(0.0F));
            WriteLine("-0.0F", toBytes(-0.0F));
            WriteLine("0.5F", toBytes(0.5F));
            WriteLine("1.0F", toBytes(1.0F));
            WriteLine("1.5F", toBytes(1.5F));
            WriteLine("2.0F", toBytes(2.0F));
            WriteLine("2.5F", toBytes(2.5F));
            WriteLine("3.0F", toBytes(3.0F));
            WriteLine("3.5F", toBytes(3.5F));
            WriteLine("-0.5F", toBytes(-0.5F));
            WriteLine("-1.0F", toBytes(-1.0F));
            WriteLine("-1.5F", toBytes(-1.5F));
            WriteLine("-2.0F", toBytes(-2.0F));
            WriteLine("-2.5F", toBytes(-2.5F));
            WriteLine("-3.0F", toBytes(-3.0F));
            WriteLine("-3.5F", toBytes(-3.5F));
            WriteLine("Single.MinValue", toBytes(Single.MinValue));
            WriteLine("Single.MaxValue", toBytes(Single.MaxValue));
            WriteLine("Single.Epsilon", toBytes(Single.Epsilon));
            WriteLine("Single.PositiveInfinity", toBytes(Single.PositiveInfinity));
            WriteLine("Single.NegativeInfinity", toBytes(Single.NegativeInfinity));
            WriteLine("Single.NaN", toBytes(Single.NaN));
            WriteLine("");
        }

        unsafe private static void PrintDouble()
        {
            Func<Double, byte[]> toBytes = x =>
            {
                var p = (byte*)&x;
                var bytes = new byte[sizeof(Double)];
                for (var n = 0; n < bytes.Length; n++)
                    bytes[n] = *p++;
                return bytes;
            };
            WriteLine("Double:");
            WriteLine("0.0D", toBytes(0.0D));
            WriteLine("-0.0D", toBytes(-0.0D));
            WriteLine("0.5D", toBytes(0.5D));
            WriteLine("1.0D", toBytes(1.0D));
            WriteLine("1.5D", toBytes(1.5D));
            WriteLine("2.0D", toBytes(2.0D));
            WriteLine("2.5D", toBytes(2.5D));
            WriteLine("3.0D", toBytes(3.0D));
            WriteLine("3.5D", toBytes(3.5D));
            WriteLine("-0.5D", toBytes(-0.5D));
            WriteLine("-1.0D", toBytes(-1.0D));
            WriteLine("-1.5D", toBytes(-1.5D));
            WriteLine("-2.0D", toBytes(-2.0D));
            WriteLine("-2.5D", toBytes(-2.5D));
            WriteLine("-3.0D", toBytes(-3.0D));
            WriteLine("-3.5D", toBytes(-3.5D));
            WriteLine("Double.MinValue", toBytes(Double.MinValue));
            WriteLine("Double.MaxValue", toBytes(Double.MaxValue));
            WriteLine("Double.Epsilon", toBytes(Double.Epsilon));
            WriteLine("Double.PositiveInfinity", toBytes(Double.PositiveInfinity));
            WriteLine("Double.NegativeInfinity", toBytes(Double.NegativeInfinity));
            WriteLine("Double.NaN", toBytes(Double.NaN));
            WriteLine("");
        }

        unsafe private static void PrintDecimal()
        {
            Func<Decimal, byte[]> toBytes = x =>
            {
                var p = (byte*)&x;
                var bytes = new byte[sizeof(Decimal)];
                for (var n = 0; n < bytes.Length; n++)
                    bytes[n] = *p++;
                return bytes;
            };
            WriteLine("Decimal:");
            WriteLine("0.0M", toBytes(0.0M));
            WriteLine("-0.0M", toBytes(-0.0M));
            WriteLine("0.12M", toBytes(0.12M));
            WriteLine("1.2M ", toBytes(1.2M));
            WriteLine("12M  ", toBytes(12M));
            WriteLine("120M ", toBytes(120M));
            WriteLine("1200M", toBytes(1200M));
            WriteLine("-0.12M", toBytes(-0.12M));
            WriteLine("-1.2M ", toBytes(-1.2M));
            WriteLine("-12M  ", toBytes(-12M));
            WriteLine("-120M ", toBytes(-120M));
            WriteLine("-1200M", toBytes(-1200M));
            WriteLine("Decimal.MinValue", toBytes(Decimal.MinValue));
            WriteLine("Decimal.MaxValue", toBytes(Decimal.MaxValue));
            WriteLine("");
        }


        private static void WriteLine(string value)
        {
            Console.WriteLine(value);
        }

        private static void WriteLine(string label, byte[] bytes)
        {
            Console.Write("{0}: 0x", label);
            var bs = (byte[])bytes.Clone();
            Array.Reverse(bs);
            foreach (var x in bs)
                Console.Write("{0:X2}", x);
            Console.WriteLine();
        }
    }
}
