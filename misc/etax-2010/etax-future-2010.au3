; 2010年分（平成22年分）「【確定申告書作成コーナー】-先物取引に係る雑所得等」入力スクリプト
; 【確定申告書作成コーナー】-先物取引に係る雑所得等 https://www.keisan.nta.go.jp/h22/syotoku/ta_subB62.jsp
; AutoIt v3 (v3.3.6.1), Windows 7 Pro. 64bit, InternetExplorer 8
; 2011/01/22 ksksts@gmail.com
#include <Array.au3>
#include <Date.au3>
#include <File.au3>
#include <IE.au3>

;$filename = "etax-future-sample.txt"  ; 取引の内訳データのファイル名（タブ区切りテキストファイル）
$filename = "etax-future-2010.txt"  ; 取引の内訳データのファイル名（タブ区切りテキストファイル）

; 取引の内訳データを読み込む
$records = ReadTSVFile($filename)

; 「先物取引に係る雑所得等」ページのIEオブジェクトを取得する
$oIE = _IEAttach ("【確定申告書作成コーナー】-先物取引に係る雑所得等")
;; 所得区分を選択する
;$oSelect = _IEGetObjByName ($oIE, "s62_Flg")
;_IEFormElementOptionSelect ($oSelect, 2) ; 0: 選択してください, 1: 事業所得用, 3: 譲渡所得用, 2: 雑所得用
; 取引の内訳を入力する
For $n = 1 To $records[0]
	InputTransaction($oIE, $records[$n])
	; もう一件入力する onclick="AddNew(); return false;"
	_IEImgClick($oIE, "img/ta_Ins_rec.gif")
	ConsoleWrite($n & ": " & _ArrayToString($records[$n]) & " (" & _Now() & ")" & @CRLF)
Next

Exit


; タブ区切りテキストファイルの内容を読み込み配列に格納する（配列の先頭要素は配列の要素数）
Func ReadTSVFile($filename)
	Dim $records
	If Not _FileReadToArray($filename, $records) Then
	   MsgBox(4096, "Error", " Error reading log to Array  error:" & @error)
	   Exit
	EndIf
	For $n = 1 To $records[0]
		$records[$n] = StringSplit($records[$n], @TAB)
	Next
	;~ For $n = 1 To $records[0]
	;~ 	ConsoleWrite(_ArrayToString($records[$n]) & @CRLF)
	;~ Next
	return $records
EndFunc

; 取引の内訳を入力する
Func InputTransaction($oIE, $record)
	; 配列の要素数が少ない場合は空の要素を追加する
	If UBound($record) < 17 Then
		ReDim $record[17]
	EndIf

	; 取引の内訳入力

	; 取引の内容
	; 種類（全角10文字以内） onBlur="jsSetList1();"
	$oText = _IEGetObjByName ($oIE, "s62_1")
	_IEFormElementSetValue ($oText, $record[1])
	$oText.fireEvent("OnBlur")
	; 決済年月日
	; var s62_k = new objmmddDate("s62_k","0101","1231"); s62_k.Allocate();
	; https://www.keisan.nta.go.jp/h22/syotoku/lib/common/script_dateselect3.js
	; function objmmddDate(ptname, frommmdd, tommdd)
	; function objmmddDate_Allocate()
	; strHTML += "<select name='" + this.PartsName + "mm' onchange='serchMM(\"" + this.PartsName + "\", \"" + this.FromMMDD + "\", \"" + this.ToMMDD + "\");jsSetListMD();' tabindex='3'>";
	; strHTML += "<select name='" + this.PartsName + "dd' onchange='jsSetListMD();'>";
	$oSelect = _IEGetObjByName ($oIE, "s62_kmm")
	_IEFormElementOptionSelect ($oSelect, $record[2])
	$oSelect = _IEGetObjByName ($oIE, "s62_kdd")
	_IEFormElementOptionSelect ($oSelect, $record[3])
	; 数量 onBlur="jsIventAddComma(document.SubForm.s62_3, 13);jsSetList3();"
	$oText = _IEGetObjByName ($oIE, "s62_3")
	_IEFormElementSetValue ($oText, $record[4])
	$oText.fireEvent("OnBlur")
	; 決済の方法（全角10文字以内）
	$oText = _IEGetObjByName ($oIE, "s62_4")
	_IEFormElementSetValue ($oText, $record[5])

	; 収入
	; 差金等決済に係る利益又は損失の額 onBlur="jsIventAddComma(document.SubForm.s62_5, 13);jsSetList5();"
	$oText = _IEGetObjByName ($oIE, "s62_5")
	_IEFormElementSetValue ($oText, $record[6])
	$oText.fireEvent("OnBlur")
	; 譲渡による収入金額 onBlur="jsIventAddComma(document.SubForm.s62_21, 13);jsSetList21();"
	$oText = _IEGetObjByName ($oIE, "s62_21")
	_IEFormElementSetValue ($oText, $record[7])
	$oText.fireEvent("OnBlur")
	; その他の収入 onBlur="jsIventAddComma(document.SubForm.s62_6, 13);jsSetList6();"
	$oText = _IEGetObjByName ($oIE, "s62_6")
	_IEFormElementSetValue ($oText, $record[8])
	$oText.fireEvent("OnBlur")

	; 必要経費等
	; 委託手数料 onBlur="jsIventAddComma(document.SubForm.s62_7, 13);jsSetList7();"
	$oText = _IEGetObjByName ($oIE, "s62_7")
	_IEFormElementSetValue ($oText, $record[9])
	$oText.fireEvent("OnBlur")
	; 譲渡による収入金額に係る取得費 onBlur="jsIventAddComma(document.SubForm.s62_22, 13);jsSetList22();"
	$oText = _IEGetObjByName ($oIE, "s62_21")
	_IEFormElementSetValue ($oText, $record[10])
	$oText.fireEvent("OnBlur")
	; s62_8_0 onBlur="jsIventAddComma(document.SubForm.s62_8, 13);jsSetList8();"
	$oText = _IEGetObjByName ($oIE, "s62_8_0")
	_IEFormElementSetValue ($oText, $record[11])
	$oText = _IEGetObjByName ($oIE, "s62_8")
	_IEFormElementSetValue ($oText, $record[12])
	$oText.fireEvent("OnBlur")
	; s62_9_0 onBlur="jsIventAddComma(document.SubForm.s62_9, 13);jsSetList9();"
	$oText = _IEGetObjByName ($oIE, "s62_9_0")
	_IEFormElementSetValue ($oText, $record[13])
	$oText = _IEGetObjByName ($oIE, "s62_9")
	_IEFormElementSetValue ($oText, $record[14])
	$oText.fireEvent("OnBlur")
	; s62_10_0 onBlur="jsIventAddComma(document.SubForm.s62_10, 13);jsSetList10();"
	$oText = _IEGetObjByName ($oIE, "s62_10_0")
	_IEFormElementSetValue ($oText, $record[15])
	$oText = _IEGetObjByName ($oIE, "s62_10")
	_IEFormElementSetValue ($oText, $record[16])
	$oText.fireEvent("OnBlur")
EndFunc
