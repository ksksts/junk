﻿// Copyright (c) 2009, ksksts (http://ksksts.blogspot.com/)
// Licensed under the MIT: http://www.opensource.org/licenses/mit-license.php
// Inspired by the original LiquidMetal by Lokesh Dhakar (http://github.com/rmm5t/liquidmetal/blob/master/liquidmetal.js).
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace AbbreviationScore
{
    public class AbbreviationScorer
    {
        #region Static fields
        private static readonly double DefaultScoreNoMatch = 0.0;
        private static readonly double DefaultScoreMatch = 1.0;
        private static readonly double DefaultScoreTrailing = 0.8;
        private static readonly double DefaultScoreTrailingButStarted = 0.9;
        private static readonly double DefaultScoreBuffer = 0.85;
        #endregion // Static fields

        #region Properties
        public double ScoreNoMatch { get; set; }
        public double ScoreMatch { get; set; }
        public double ScoreTrailing { get; set; }
        public double ScoreTrailingButStarted { get; set; }
        public double ScoreBuffer { get; set; }
        #endregion // Properties

        #region Constructors
        public AbbreviationScorer()
            : this(DefaultScoreNoMatch, DefaultScoreMatch, 
                   DefaultScoreTrailing, DefaultScoreTrailingButStarted, 
                   DefaultScoreBuffer) { }

        public AbbreviationScorer(double scoreNoMatch, double scoreMatch, 
                                  double scoreTrailing, double scoreTrailingButStarted, 
                                  double scoreBuffer)
        {
            ScoreNoMatch = scoreNoMatch;
            ScoreMatch = scoreMatch;
            ScoreTrailing = scoreTrailing;
            ScoreTrailingButStarted = scoreTrailingButStarted;
            ScoreBuffer = scoreBuffer;
        }
        #endregion // Constructors

        #region Static methods
        private static double[] FillArray(double[] array, double value, int from, int to)
        {
            Debug.Assert(from >= 0);
            Debug.Assert(to <= array.Length);
            for (int n = from; n < to; n++) array[n] = value;
            return array;
        }
        #endregion // Static methods

        #region Methods
        public double Compute(string @string, string abbreviation)
        {
            if (abbreviation.Length == 0) return ScoreTrailing;
            if (abbreviation.Length > @string.Length) return ScoreNoMatch;

            var scores = BuildScoreArray(@string, abbreviation);
            double sum = scores.Sum();
            double score = sum / scores.Length;
            return score;
        }

        private double[] BuildScoreArray(string @string, string abbreviation)
        {
            Debug.Assert(abbreviation.Length > 0);
            Debug.Assert(abbreviation.Length <= @string.Length);

            var scores = new double[@string.Length];
            string lower = @string.ToLower();
            string chars = abbreviation.ToLower();

            int lastIndex = -1;
            bool started = false;

            foreach (char c in chars)
            {
                int index = lower.IndexOf(c, lastIndex + 1);
                if (index < 0) return FillArray(scores, ScoreNoMatch, 0, scores.Length);
                if (index == 0) started = true;

                if (index > 0 && char.IsWhiteSpace(@string, index - 1))
                {
                    scores[index - 1] = 1.0;
                    FillArray(scores, ScoreBuffer, lastIndex + 1, index - 1);
                }
                else if (char.IsUpper(@string, index))
                {
                    FillArray(scores, ScoreBuffer, lastIndex + 1, index);
                }
                else
                {
                    FillArray(scores, ScoreNoMatch, lastIndex + 1, index);
                }

                scores[index] = ScoreMatch;
                lastIndex = index;
            }

            double trailingScore = started ? ScoreTrailingButStarted : ScoreTrailing;
            FillArray(scores, trailingScore, lastIndex + 1, scores.Length);
            return scores;
        }
        #endregion // Methods
    }
}
