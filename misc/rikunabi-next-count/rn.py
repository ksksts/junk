#!/usr/bin/env python 
# -*- encoding: utf-8 -*-

import sys
import os.path
import re
import email, email.Utils
import mailbox

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "usage: %s mboxfile" % os.path.basename(sys.argv[0])
        exit(1)
    pattern = re.compile(ur"(?:すべての新着・更新求人：(\d+))|(?:(\d+)件の求人が今週掲載開始。)|(?:▼新着・更新求人：(\d+))")
    file = open(sys.argv[1])
    mbox = mailbox.UnixMailbox(file, email.message_from_file)
    for message in mbox:
        date = email.Utils.parsedate(message["Date"])
        text = message.get_payload().decode("iso-2022-jp")
        match = pattern.search(text)
        if match == None:
            continue
        count = [x for x in match.groups() if x != None]
        if len(count) > 0:
            print u"%04d/%02d/%02d\t%s" % (date[0], date[1], date[2], count[0], )
