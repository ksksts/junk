﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestExpressionAssertion;

namespace MSTestExpressionAssertionTest
{
    [TestClass]
    public class AssertExSample
    {
        public AssertExSample() { }

        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        [TestMethod]
        public void Test00()
        {
            var p = new Point(1.0, 2.0);
            var tole = 1.0e-12;
            AssertEx.IsFalse(() => Math.Abs(p.CalculateDistance(Point.ORIGIN) - Math.Sqrt(5.0)) <= tole);
        }

        [TestMethod]
        public void Test01()
        {
            var m = Matrix.MakeRotation(Math.PI * 0.25);
            var tole = 1.0e-12;
            AssertEx.IsFalse(() => m.Transform(new Point(1.0, 0.0)).CalculateDistance(new Point(Math.Sqrt(2.0) * 0.5, Math.Sqrt(2.0) * 0.5)) <= tole);
        }

        [TestMethod]
        public void Test02()
        {
            AssertEx.IsTrue(() => string.Format("{0}", null) == "");
        }
    }
}
