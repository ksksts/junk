﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace BinaryRepresentation
{
    class Program
    {
        static void Main(string[] args)
        {
            var sw = new Stopwatch();
            var random = new Random(0);
            var num = 1000000;
            var vals = new double[num];
            for (var n = 0; n < vals.Length; n++)
                vals[n] = random.NextDouble();

            // BitConverter.DoubleToInt64Bits
            var bc = new long[vals.Length];
            {
                sw.Reset();
                sw.Start();
                for (var n = 0; n < vals.Length; n++)
                    bc[n] = BitConverter.DoubleToInt64Bits(vals[n]);
                sw.Stop();
                Console.WriteLine("BitConverter.DoubleToInt64Bits: {0}", sw.ElapsedMilliseconds);
            }

            // unsafe cast
            var uc = new long[vals.Length];
            unsafe
            {
                sw.Reset();
                sw.Start();
                for (var n = 0; n < vals.Length; n++)
                {
                    var val = vals[n];
                    uc[n] = *(long*)&val;
                }
                sw.Stop();
                Console.WriteLine("unsafe cast: {0}", sw.ElapsedMilliseconds);
            }

            // correctness
            for (var n = 0; n < vals.Length; n++)
                if (bc[n] != uc[n])
                    Console.WriteLine("bc[{0}]: {1:X}, uc[{0}]: {2:X}", n, bc[n], uc[n]);
        }
    }
}
