﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using MSTestExpressionAssertion.Core;

namespace MSTestExpressionAssertion
{
    public static class AssertEx
    {
        public static void IsTrue(Expression<Func<bool>> condition)
        {
            IsTrue(condition, null, null);
        }

        public static void IsTrue(Expression<Func<bool>> condition, string message)
        {
            IsTrue(condition, message, null);
        }

        public static void IsTrue(Expression<Func<bool>> condition, string message, params object[] parameters)
        {
            if (condition == null)
                throw new ArgumentException("condition");
            var interpreter = new ExpressionInterpreter<bool>();
            var result = interpreter.Eval(condition);
            if (!result)
            {
                var msg = "AssertEx.IsTrue failed." + Environment.NewLine;
                msg += BuildMessage(interpreter, message, parameters);
                throw new AssertExFailedException(msg);
            }
        }

        public static void IsFalse(Expression<Func<bool>> condition)
        {
            IsFalse(condition, null, null);
        }

        public static void IsFalse(Expression<Func<bool>> condition, string message)
        {
            IsFalse(condition, message, null);
        }

        public static void IsFalse(Expression<Func<bool>> condition, string message, params object[] parameters)
        {
            if (condition == null)
                throw new ArgumentException("condition");
            var interpreter = new ExpressionInterpreter<bool>();
            var result = interpreter.Eval(condition);
            if (result)
            {
                var msg = "AssertEx.IsFalse failed." + Environment.NewLine;
                msg += BuildMessage(interpreter, message, parameters);
                throw new AssertExFailedException(msg);
            }
        }

        private static string BuildMessage(ExpressionInterpreter<bool> interpreter, string message, params object[] parameters)
        {
            var builder = new StringBuilder();
            if (message != null)
            {
                if (parameters != null)
                    builder.AppendFormat(message, parameters);
                else
                    builder.Append(message);
            }
            foreach (var labeledValue in interpreter.LabeledValues)
            {
                if (builder.Length > 0)
                    builder.AppendLine();
                var level = labeledValue.First;
                var label = labeledValue.Second;
                var result = labeledValue.Third != null ? labeledValue.Third.ToString() : "null";
                label = string.Format("{0}{1}: ", new string(' ', level), label);
                result = result.Replace(Environment.NewLine, 
                                        Environment.NewLine + new string(' ', label.Length));
                builder.Append(label).Append(result);
            }
            return builder.ToString();
        }
    }
}
