﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

using MiniTwitter.Net.Twitter;

namespace MiniTwitter
{
    [Serializable]
    public class Account : PropertyChangedBase, IEditableObject, IEquatable<Account>
    {
        private string username;

        [XmlAttribute]
        public string Username
        {
            get { return username; }
            set
            {
                if (username != value)
                {
                    username = value;
                    OnPropertyChanged("Username");
                }
            }
        }

        private string password;

        [XmlAttribute]
        public string Password
        {
            get { return password; }
            set
            {
                if (password != value)
                {
                    password = value;
                    OnPropertyChanged("Password");
                    OnPropertyChanged("HiddenPassword");
                }
            }
        }

        [XmlIgnore]
        public string HiddenPassword
        {
            get { return new string('●', password.Length); }
        }

        private Account copy;

        public void BeginEdit()
        {
            if (copy == null)
            {
                copy = new Account();
            }
            copy.Username = this.Username;
            copy.Password = this.Password;
        }

        public void CancelEdit()
        {
            this.Username = copy.Username;
            this.Password = copy.Password;
        }

        public void EndEdit()
        {
            copy.Username = "";
            copy.Password = "";
        }

        #region IEquatable<Account> メンバ

        public bool Equals(Account other)
        {
            if (other == null)
                return false;
            return username == other.username && password == other.password;
        }

        #endregion
    }
}
