﻿// Copyright (c) 2009, ksksts <http://ksksts.blogspot.com/>
// Licensed under the zlib/libpng: http://www.opensource.org/licenses/zlib-license.php
// Inspired by STLport <http://stlport.sourceforge.net/>
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace MergeSort
{
    public class InPlaceMergeSorter
    {
        public static void Sort<T>(IList<T> list)
        {
            var comparer = Comparer<T>.Default;
            if (comparer == null)
                throw new InvalidOperationException("The default comparer Comparer<T>.Default cannot find an implementation of the IComparable<T> generic interface or the IComparable interface for type T.");
            Sort(list, 0, list.Count, comparer.Compare);
        }

        public static void Sort<T>(IList<T> list, IComparer<T> comparer)
        {
            Sort(list, 0, list.Count, comparer.Compare);
        }

        public static void Sort<T>(IList<T> list, Comparison<T> comparison)
        {
            Sort(list, 0, list.Count, comparison);
        }

        public static void Sort<T>(IList<T> list, int index, int count, Comparison<T> comparison)
        {
            var sorter = new Sorter<T>();
            sorter.Sort(list, index, count, comparison);
        }


        private class Sorter<T>
        {
            private IList<T> list;
            private Comparison<T> comparison;

            public Sorter() { }

            public void Sort(IList<T> list, int index, int count, Comparison<T> comparison)
            {
                this.list = list;
                this.comparison = comparison;
                Sort(index, index + count);
            }

            private void Sort(int first, int last)
            {
                Debug.Assert(first <= last);
                if (last - first < 15)
                {
                    InsertionSort(first, last);
                    return;
                }
                var middle = (first + last) / 2;
                Sort(first, middle);
                Sort(middle, last);
                Merge(first, middle, last);
            }

            private void Merge(int first, int middle, int last)
            {
                Debug.Assert(first <= middle);
                Debug.Assert(middle <= last);
                var left = middle - first;
                var right = last - middle;
                if (left == 0 || right == 0)
                    return;
                if (left == 1 && right == 1)
                {
                    if (comparison(list[first + 1], list[first]) < 0)
                        Swap(first + 1, first);
                    return;
                }
                var leftCut = 0;
                var rightCut = 0;
                if (left > right)
                {
                    leftCut = first + left / 2;
                    rightCut = LowerBound(middle, last, list[leftCut]);
                }
                else
                {
                    rightCut = middle + right / 2;
                    leftCut = UpperBound(first, middle, list[rightCut]);
                }
                middle = Rotate(leftCut, middle, rightCut);
                Merge(first, leftCut, middle);
                Merge(middle, rightCut, last);
            }

            private int Rotate(int first, int middle, int last)
            {
                Debug.Assert(first <= middle);
                Debug.Assert(middle <= last);
                var result = first + (last - middle);
                if (first == middle || middle == last)
                    return result;
                var left = middle - first;
                var gcd = GCD(last - first, left);
                for (var n = 0; n < gcd; n++)
                {
                    var p = first + n;
                    var q = middle + n;
                    var tv = list[p];
                    while (q != first + n)
                    {
                        list[p] = list[q];
                        p = q;
                        q += left;
                        if (q >= last)
                            q = q - last + first;
                    }
                    list[p] = tv;
                }
                return result;
            }

            private void InsertionSort(int first, int last)
            {
                for (var n = first + 1; n < last; n++)
                {
                    for (var m = n; m > first; m--)
                    {
                        if (comparison(list[m], list[m - 1]) < 0)
                            Swap(m, m - 1);
                        else
                            break;
                    }
                }
            }

            private int LowerBound(int first, int last, T value)
            {
                var count = last - first;
                while (count > 0)
                {
                    var half = count / 2;
                    var middle = first + half;
                    if (comparison(list[middle], value) < 0)
                    {
                        first = middle + 1;
                        count = count - half - 1;
                    }
                    else
                        count = half;
                }
                return first;
            }

            private int UpperBound(int first, int last, T value)
            {
                var count = last - first;
                while (count > 0)
                {
                    var half = count / 2;
                    var middle = first + half;
                    if (comparison(value, list[middle]) < 0)
                        count = half;
                    else
                    {
                        first = middle + 1;
                        count = count - half - 1;
                    }
                }
                return first;
            }

            private void Swap(int x, int y)
            {
                var t = list[x];
                list[x] = list[y];
                list[y] = t;
            }

            private static int GCD(int m, int n)
            {
                while (n != 0)
                {
                    var t = m % n;
                    m = n;
                    n = t;
                }
                return m;
            }
        }
    }
}
