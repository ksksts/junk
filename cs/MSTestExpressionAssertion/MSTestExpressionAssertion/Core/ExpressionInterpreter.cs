// Copyright 2005-2009 Gallio Project - http://www.gallio.org/
// Portions Copyright 2000-2004 Jonathan de Halleux
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// modified by ksksts. see modified-files.diff

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq.Expressions;
using Gallio.Common;
using Gallio.Common.Linq;

namespace MSTestExpressionAssertion.Core
{
    public class ExpressionInterpreter<TResult> : ExpressionInstrumentor
    {
        private Trace currentTrace;

        public Expression<Func<TResult>> Condition { get; private set; }
        public TResult Result { get; private set; }
        public Trace RootTrace { get; private set; }
        public List<Triple<int, string, object>> LabeledValues { get; private set; }

        public ExpressionInterpreter() { }

        public TResult Eval(Expression<Func<TResult>> condition)
        {
            currentTrace = new Trace(null);
            Condition = condition;
            Result = default(TResult);
            RootTrace = currentTrace;
            LabeledValues = new List<Triple<int, string, object>>();

            if (condition == null)
                throw new ArgumentNullException("condition");

            Result = Compile(condition)();
            Describe(currentTrace);

            RootTrace = RootTrace.Children[0];
            return Result;
        }

        [DebuggerHidden, DebuggerStepThrough]
        protected override T Intercept<T>(Expression expr, Func<T> continuation)
        {
            var parentTrace = currentTrace;
            var trace = new Trace(expr);

            currentTrace.AddChild(trace);
            currentTrace = trace;

            var result = base.Intercept(expr, continuation);
            trace.Result = result;

            currentTrace = parentTrace;

            return result;
        }

        private void Describe(Trace trace)
        {
            // Skip trivial nodes in the trace tree that are not of much interest.
            while (IsTrivialExpression(trace.Expression) && trace.Children.Count == 1)
                trace = trace.Children[0];

            var labeledTraces = new List<Pair<int, Trace>>();
            AddLabeledTraces(labeledTraces, 0, trace);

            LabeledValues.Clear();
            foreach (var labeledTrace in labeledTraces)
            {
                var level = labeledTrace.First;
                var label = ExpressionFormatter.Format(labeledTrace.Second.Expression);
                var result = labeledTrace.Second.Result;
                LabeledValues.Add(new Triple<int, string, object>(level, label, result));
            }
        }

        private static void AddLabeledTraces(List<Pair<int, Trace>> labeledTraces, int level, Trace trace)
        {
            var expr = trace.Expression;
            if (level == 0 || !(expr is ConstantExpression))
            {
                labeledTraces.Add(new Pair<int, Trace>(level, trace));
                foreach (var child in trace.Children)
                    AddLabeledTraces(labeledTraces, level + 1, child);
            }
        }

        private static bool IsTrivialExpression(Expression expr)
        {
            return expr == null || expr.NodeType == ExpressionType.Not;
        }


        public sealed class Trace
        {
            public Trace(Expression expression)
            {
                Expression = expression;
                Children = new List<Trace>();
            }

            public Expression Expression { get; private set; }
            public object Result { get; set; }
            public IList<Trace> Children { get; private set; }

            public void AddChild(Trace child)
            {
                Children.Add(child);
            }
        }
    }
}
