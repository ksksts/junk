﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using IntroSort;

namespace Benchmark
{
    class Program
    {
        static void Main(string[] args)
        {
            for (var size = 1000; size <= 1000000; size *= 10)
                Run(size);
        }

        static void Run(int size)
        {
            Console.WriteLine("size: {0}", size);

            var count = 10;
            var sw = new Stopwatch();
            var introSortTicks = 0L;
            var arraySortTicks = 0L;
            for (var c = 0; c < count; c++)
            {
                var random = new Random(c);
                var list = Enumerable.Range(0, size)
                                     .Select(_ => random.Next(Int32.MinValue, Int32.MaxValue))
                                     .ToArray();

                // intro sort
                var introSortResult = (IList<Int32>)list.Clone();
                sw.Reset();
                sw.Start();
                IntroSorter.Sort(introSortResult);
                sw.Stop();
                introSortTicks += sw.ElapsedTicks;

                // Array.Sort
                var arraySortResult = list.ToArray();
                sw.Reset();
                sw.Start();
                Array.Sort(arraySortResult);
                sw.Stop();
                arraySortTicks += sw.ElapsedTicks;

                // correctness
                for (var n = 0; n < size; n++)
                {
                    if (introSortResult[n] != arraySortResult[n])
                        Console.WriteLine("introSortResult[{0}]: {1}, arraySortResult[{0}]: {2}", n, introSortResult[n], arraySortResult[n]);
                }
            }

            Console.WriteLine("IntroSorter.Sort: {0} ({1:F})", introSortTicks / count, introSortTicks / (double)arraySortTicks);
            Console.WriteLine("Array.Sort: {0} ({1:F})", arraySortTicks / count, arraySortTicks / (double)arraySortTicks);
        }
    }
}
