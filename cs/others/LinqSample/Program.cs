﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace LinqSample
{
    class Program
    {
        static void Main(string[] args)
        {
            ManipulateArray();
        }

        static private void ManipulateArray()
        {
            // initialize array
            {
                var size = 10;
                var x = 3;
                int[] ary = Enumerable.Range(0, size).Select(_ => x).ToArray();
                Write(ary);
            }

            // fill array
            {
                var size = 10;
                var ary = new int[size];
                var x = 3;
                ary = ary.Select(_ => x).ToArray();
                Write(ary);
            }
        }

        static private void Write<T>(IEnumerable<T> enumerable)
        {
            foreach (var x in enumerable)
                Console.Write("{0}, ", x);
            Console.Write("\n");
        }
    }
}
