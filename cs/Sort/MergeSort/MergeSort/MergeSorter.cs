﻿// Copyright (c) 2009, ksksts <http://ksksts.blogspot.com/>
// Licensed under the zlib/libpng: http://www.opensource.org/licenses/zlib-license.php
// Inspired by STLport <http://stlport.sourceforge.net/>
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace MergeSort
{
    public class MergeSorter
    {
        public static void Sort<T>(IList<T> list)
        {
            var comparer = Comparer<T>.Default;
            if (comparer == null)
                throw new InvalidOperationException("The default comparer Comparer<T>.Default cannot find an implementation of the IComparable<T> generic interface or the IComparable interface for type T.");
            Sort(list, 0, list.Count, comparer.Compare);
        }

        public static void Sort<T>(IList<T> list, IComparer<T> comparer)
        {
            Sort(list, 0, list.Count, comparer.Compare);
        }

        public static void Sort<T>(IList<T> list, Comparison<T> comparison)
        {
            Sort(list, 0, list.Count, comparison);
        }

        public static void Sort<T>(IList<T> list, int index, int count, Comparison<T> comparison)
        {
            var sorter = new Sorter<T>();
            sorter.Sort(list, index, count, comparison);
        }


        private class Sorter<T>
        {
            private IList<T> list;
            private Comparison<T> comparison;
            private readonly int chunkSize = 7;
            private IList<T> buffer;

            public Sorter() { }

            public void Sort(IList<T> list, int index, int count, Comparison<T> comparison)
            {
                this.list = list;
                this.comparison = comparison;
                this.buffer = new T[count];
                Sort(index, index + count);
            }

            private void Sort(int first, int last)
            {
                Debug.Assert(first <= last);
                var middle = (first + last) / 2;
                SortWithBuffer(first, middle);
                SortWithBuffer(middle, last);
                var half = middle - first;
                for (var n = 0; n < half; n++)
                    buffer[n] = list[first + n];
                Merge(buffer, 0, half, list, middle, last, list, first);
            }

            private void SortWithBuffer(int first, int last)
            {
                Debug.Assert(first <= last);
                var stepSize = chunkSize;
                ChunkInsertionSort(first, last, stepSize);
                var count = last - first;
                while (stepSize < count)
                {
                    SortLoop(list, first, last, buffer, 0, stepSize);
                    stepSize *= 2;
                    SortLoop(buffer, 0, count, list, first, stepSize);
                    stepSize *= 2;
                }
            }

            private void SortLoop(IList<T> list, int first, int last, 
                                  IList<T> resultList, int resultFirst, 
                                  int stepSize)
            {
                Debug.Assert(first <= last);
                var twoStep = stepSize * 2;
                while (last - first >= twoStep)
                {
                    resultFirst = Merge(list, first, first + stepSize, 
                                        list, first + stepSize, first + twoStep,
                                        resultList, resultFirst);
                    first += twoStep;
                }
                stepSize = Math.Min(last - first, stepSize);
                Merge(list, first, first + stepSize, 
                      list, first + stepSize, last, 
                      resultList, resultFirst);
            }

            private int Merge(IList<T> leftList, int leftFirst, int leftLast,
                              IList<T> rightList, int rightFirst, int rightLast, 
                              IList<T> resultList, int resultFirst)
            {
                Debug.Assert(leftFirst <= leftLast);
                Debug.Assert(rightFirst <= rightLast);
                while (leftFirst < leftLast && rightFirst < rightLast)
                {
                    if (comparison(rightList[rightFirst], leftList[leftFirst]) < 0)
                    {
                        resultList[resultFirst] = rightList[rightFirst];
                        rightFirst++;
                    }
                    else
                    {
                        resultList[resultFirst] = leftList[leftFirst];
                        leftFirst++;
                    }
                    resultFirst++;
                }
                while (leftFirst < leftLast)
                {
                    resultList[resultFirst] = leftList[leftFirst];
                    leftFirst++;
                    resultFirst++;
                }
                while (rightFirst < rightLast)
                {
                    resultList[resultFirst] = rightList[rightFirst];
                    rightFirst++;
                    resultFirst++;
                }
                return resultFirst;
            }

            private void ChunkInsertionSort(int first, int last, int chunkSize)
            {
                Debug.Assert(first <= last);
                while (last - first >= chunkSize)
                {
                    InsertionSort(first, first + chunkSize);
                    first += chunkSize;
                }
                InsertionSort(first, last);
            }

            private void InsertionSort(int first, int last)
            {
                Debug.Assert(first <= last);
                for (var n = first + 1; n < last; n++)
                {
                    for (var m = n; m > first; m--)
                    {
                        if (comparison(list[m], list[m - 1]) < 0)
                        {
                            var tv = list[m];
                            list[m] = list[m - 1];
                            list[m - 1] = tv;
                        }
                        else
                            break;
                    }
                }
            }
        }
    }
}
