﻿// Copyright (c) 2009, ksksts <http://ksksts.blogspot.com/>
// Licensed under the zlib/libpng: http://www.opensource.org/licenses/zlib-license.php
// Inspired by 
//   Radix Sort Revisited <http://codercorner.com/RadixSortRevisited.htm> and
//   stereopsis : graphics : radix tricks <http://www.stereopsis.com/radix.html>
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace RadixSort
{
    public class RadixSorter
    {
        public enum SortOrder
        {
            Ascending,
            Descending, 
        }


        public static void Sort<T>(IList<T> list, Func<T, UInt32> converter, SortOrder sortOrder)
        {
            Debug.Assert(sortOrder == SortOrder.Ascending || sortOrder == SortOrder.Descending);
            var ascending = sortOrder == SortOrder.Ascending;

            var tables = new int[sizeof(UInt32)][];
            for (var p = 0; p < tables.Length; p++)
                tables[p] = new int[0xFF + 1];

            // histgramming
            var histgramOffset = ascending ? 1 : -1;
            foreach (var x in list)
            {
                var y = converter(x);
                for (var p = 0; p < tables.Length; p++)
                {
                    tables[p][(y + histgramOffset) & 0xFF]++;
                    y = y >> 8;
                }
            }

            // sum the histgrams
            if (ascending)
            {
                foreach (var table in tables)
                {
                    table[0] = 0;
                    for (var n = 1; n < table.Length; n++)
                        table[n] += table[n - 1];
                }
            }
            else
            {
                foreach (var table in tables)
                {
                    table[0xFF] = 0;
                    for (var n = 0xFE; n >= 0x00; n--)
                        table[n] += table[n + 1];
                }
            }

            // read/write histgram, copy
            IList<T> array = new T[list.Count];
            Action swap = () => { var temp = list; list = array; array = temp; };
            for (var p = 0; p < tables.Length; p++)
            {
                var table = tables[p];
                foreach (var x in list)
                {
                    var y = (converter(x) >> p * 8) & 0xFF;
                    array[table[y]] = x;
                    table[y]++;
                }
                swap();
            }
        }

        public static void Sort<T>(IList<T> list, Func<T, UInt64> converter, SortOrder sortOrder)
        {
            Debug.Assert(sortOrder == SortOrder.Ascending || sortOrder == SortOrder.Descending);
            var ascending = sortOrder == SortOrder.Ascending;

            var tables = new int[sizeof(UInt64)][];
            for (var p = 0; p < tables.Length; p++)
                tables[p] = new int[0xFF + 1];

            // histgramming
            var histgramOffset = (UInt64)(ascending ? 1 : -1);
            foreach (var x in list)
            {
                var y = converter(x);
                for (var p = 0; p < tables.Length; p++)
                {
                    tables[p][(y + histgramOffset) & 0xFF]++;
                    y = y >> 8;
                }
            }

            // sum the histgrams
            if (ascending)
            {
                foreach (var table in tables)
                {
                    table[0] = 0;
                    for (var n = 1; n < table.Length; n++)
                        table[n] += table[n - 1];
                }
            }
            else
            {
                foreach (var table in tables)
                {
                    table[0xFF] = 0;
                    for (var n = 0xFE; n >= 0x00; n--)
                        table[n] += table[n + 1];
                }
            }

            // read/write histgram, copy
            IList<T> array = new T[list.Count];
            Action swap = () => { var temp = list; list = array; array = temp; };
            for (var p = 0; p < tables.Length; p++)
            {
                var table = tables[p];
                foreach (var x in list)
                {
                    var y = (converter(x) >> p * 8) & 0xFF;
                    array[table[y]] = x;
                    table[y]++;
                }
                swap();
            }
        }

        public static void Sort<T>(IList<T> list, Func<T, Int32> converter, SortOrder sortOrder)
        {
            Debug.Assert(sortOrder == SortOrder.Ascending || sortOrder == SortOrder.Descending);
            var ascending = sortOrder == SortOrder.Ascending;

            var tables = new int[sizeof(Int32)][];
            for (var p = 0; p < tables.Length; p++)
                tables[p] = new int[0xFF + 1];

            // histgramming
            var histgramOffset = ascending ? 1 : -1;
            foreach (var x in list)
            {
                var y = converter(x);
                for (var p = 0; p < tables.Length; p++)
                {
                    tables[p][(y + histgramOffset) & 0xFF]++;
                    y = y >> 8;
                }
            }

            // sum the histgrams
            if (ascending)
            {
                for (var p = 0; p < tables.Length - 1; p++)
                {
                    var table = tables[p];
                    table[0] = 0;
                    for (var n = 1; n < table.Length; n++)
                        table[n] += table[n - 1];
                }
                {
                    var table = tables[tables.Length - 1];
                    table[0x80] = 0;
                    for (var n = 0x81; n <= 0xFF; n++)
                        table[n] += table[n - 1];
                    table[0x00] += table[0xFF];
                    for (var n = 0x01; n < 0x80; n++)
                        table[n] += table[n - 1];
                }
            }
            else
            {
                for (var p = 0; p < tables.Length - 1; p++)
                {
                    var table = tables[p];
                    table[0xFF] = 0;
                    for (var n = 0xFE; n >= 0x00; n--)
                        table[n] += table[n + 1];
                }
                {
                    var table = tables[tables.Length - 1];
                    table[0x7F] = 0;
                    for (var n = 0x7E; n >= 0x00; n--)
                        table[n] += table[n + 1];
                    table[0xFF] += table[0x00];
                    for (var n = 0xFE; n >= 0x80; n--)
                        table[n] += table[n + 1];
                }
            }

            // read/write histgram, copy
            IList<T> array = new T[list.Count];
            Action swap = () => { var temp = list; list = array; array = temp; };
            for (var p = 0; p < tables.Length; p++)
            {
                var table = tables[p];
                foreach (var x in list)
                {
                    var y = (converter(x) >> p * 8) & 0xFF;
                    array[table[y]] = x;
                    table[y]++;
                }
                swap();
            }
        }

        public static void Sort<T>(IList<T> list, Func<T, Int64> converter, SortOrder sortOrder)
        {
            Debug.Assert(sortOrder == SortOrder.Ascending || sortOrder == SortOrder.Descending);
            var ascending = sortOrder == SortOrder.Ascending;

            var tables = new int[sizeof(Int64)][];
            for (var p = 0; p < tables.Length; p++)
                tables[p] = new int[0xFF + 1];

            // histgramming
            var histgramOffset = (Int64)(ascending ? 1 : -1);
            foreach (var x in list)
            {
                var y = converter(x);
                for (var p = 0; p < tables.Length; p++)
                {
                    tables[p][(y + histgramOffset) & 0xFF]++;
                    y = y >> 8;
                }
            }

            // sum the histgrams
            if (ascending)
            {
                for (var p = 0; p < tables.Length - 1; p++)
                {
                    var table = tables[p];
                    table[0] = 0;
                    for (var n = 1; n < table.Length; n++)
                        table[n] += table[n - 1];
                }
                {
                    var table = tables[tables.Length - 1];
                    table[0x80] = 0;
                    for (var n = 0x81; n <= 0xFF; n++)
                        table[n] += table[n - 1];
                    table[0x00] += table[0xFF];
                    for (var n = 0x01; n < 0x80; n++)
                        table[n] += table[n - 1];
                }
            }
            else
            {
                for (var p = 0; p < tables.Length - 1; p++)
                {
                    var table = tables[p];
                    table[0xFF] = 0;
                    for (var n = 0xFE; n >= 0x00; n--)
                        table[n] += table[n + 1];
                }
                {
                    var table = tables[tables.Length - 1];
                    table[0x7F] = 0;
                    for (var n = 0x7E; n >= 0x00; n--)
                        table[n] += table[n + 1];
                    table[0xFF] += table[0x00];
                    for (var n = 0xFE; n >= 0x80; n--)
                        table[n] += table[n + 1];
                }
            }

            // read/write histgram, copy
            IList<T> array = new T[list.Count];
            Action swap = () => { var temp = list; list = array; array = temp; };
            for (var p = 0; p < tables.Length; p++)
            {
                var table = tables[p];
                foreach (var x in list)
                {
                    var y = (converter(x) >> p * 8) & 0xFF;
                    array[table[y]] = x;
                    table[y]++;
                }
                swap();
            }
        }

        unsafe public static void Sort<T>(IList<T> list, Func<T, Single> converter, SortOrder sortOrder)
        {
            Debug.Assert(sortOrder == SortOrder.Ascending || sortOrder == SortOrder.Descending);
            var ascending = sortOrder == SortOrder.Ascending;

            var tables = new int[sizeof(Single)][];
            for (var p = 0; p < tables.Length; p++)
                tables[p] = new int[0xFF + 1];

            // histgramming
            var histgramOffset = ascending ? 1 : -1;
            foreach (var x in list)
            {
                var t = converter(x);
                var y = *(Int32*)&t;
                y ^= -(Int32)((UInt32)y >> 31) | unchecked((Int32)0x80000000);  // flip
                for (var p = 0; p < tables.Length; p++)
                {
                    tables[p][(y + histgramOffset) & 0xFF]++;
                    y = y >> 8;
                }
            }

            // sum the histgrams
            if (ascending)
            {
                foreach (var table in tables)
                {
                    table[0] = 0;
                    for (var n = 1; n < table.Length; n++)
                        table[n] += table[n - 1];
                }
            }
            else
            {
                foreach (var table in tables)
                {
                    table[0xFF] = 0;
                    for (var n = 0xFE; n >= 0x00; n--)
                        table[n] += table[n + 1];
                }
            }

            // read/write histgram, copy
            IList<T> array = new T[list.Count];
            Action swap = () => { var temp = list; list = array; array = temp; };
            for (var p = 0; p < tables.Length; p++)
            {
                var table = tables[p];
                foreach (var x in list)
                {
                    var t = converter(x);
                    var y = *(Int32*)&t;
                    y ^= -(Int32)((UInt32)y >> 31) | unchecked((Int32)0x80000000);  // flip
                    y = (y >> p * 8) & 0xFF;
                    array[table[y]] = x;
                    table[y]++;
                }
                swap();
            }
        }

        public static void Sort<T>(IList<T> list, Func<T, Double> converter, SortOrder sortOrder)
        {
            Debug.Assert(sortOrder == SortOrder.Ascending || sortOrder == SortOrder.Descending);
            var ascending = sortOrder == SortOrder.Ascending;

            var tables = new int[sizeof(Double)][];
            for (var p = 0; p < tables.Length; p++)
                tables[p] = new int[0xFF + 1];

            // histgramming
            var histgramOffset = (Int64)(ascending ? 1 : -1);
            foreach (var x in list)
            {
                var y = BitConverter.DoubleToInt64Bits(converter(x));
                y ^= -(Int64)((UInt64)y >> 63) | unchecked((Int64)0x8000000000000000);  // flip
                for (var p = 0; p < tables.Length; p++)
                {
                    tables[p][(y + histgramOffset) & 0xFF]++;
                    y = y >> 8;
                }
            }

            // sum the histgrams
            if (ascending)
            {
                foreach (var table in tables)
                {
                    table[0] = 0;
                    for (var n = 1; n < table.Length; n++)
                        table[n] += table[n - 1];
                }
            }
            else
            {
                foreach (var table in tables)
                {
                    table[0xFF] = 0;
                    for (var n = 0xFE; n >= 0x00; n--)
                        table[n] += table[n + 1];
                }
            }

            // read/write histgram, copy
            IList<T> array = new T[list.Count];
            Action swap = () => { var temp = list; list = array; array = temp; };
            for (var p = 0; p < tables.Length; p++)
            {
                var table = tables[p];
                foreach (var x in list)
                {
                    var y = BitConverter.DoubleToInt64Bits(converter(x));
                    y ^= -(Int64)((UInt64)y >> 63) | unchecked((Int64)0x8000000000000000);  // flip
                    y = (y >> p * 8) & 0xFF;
                    array[table[y]] = x;
                    table[y]++;
                }
                swap();
            }
        }
    }
}
