﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IListArray
{
    //public class IListArrayWrapper<T> where T : struct
    //{
    //    public T[] Source { get; set; }
    //    unsafe public T this[int index]
    //    {
    //        get
    //        {
    //            fixed (T* p = &Source[0])
    //                return *(p + index);
    //        }
    //        set
    //        {
    //            fixed (T* p = &Source[0])
    //                *(p + index) = value;
    //        }
    //    }
    //    public IListArrayWrapper(IList<T> source)
    //    {
    //        Source = (T[])source;
    //    }
    //}
    public class IListArrayWrapper
    {
        public int[] Source { get; set; }
        unsafe public int this[int index]
        {
            get
            {
                fixed (int* p = &Source[0])
                    return *(p + index);
            }
            set
            {
                fixed (int* p = &Source[0])
                    *(p + index) = value;
            }
        }
        public IListArrayWrapper(IList<int> source)
        {
            Source = (int[])source;
        }
    }
}
