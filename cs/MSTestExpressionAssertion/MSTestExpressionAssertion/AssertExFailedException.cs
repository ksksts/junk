﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MSTestExpressionAssertion
{
    [Serializable]
    public class AssertExFailedException : AssertFailedException
    {
        public AssertExFailedException() : base() { }
        public AssertExFailedException(string msg) : base(msg) { }
        protected AssertExFailedException(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public AssertExFailedException(string msg, Exception ex) : base(msg, ex) { }

        public override string StackTrace
        {
            get
            {
                var lines = base.StackTrace.Split(new string[] { Environment.NewLine, }, StringSplitOptions.None);
                var index = Array.FindIndex(lines, x => !x.Contains("MSTestExpressionAssertion."));
                var st = string.Join(Environment.NewLine, lines, index, lines.Length - index);
                return st;
            }
        }
    }
}
