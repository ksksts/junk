﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RadixSort;
using MergeSort;
using IntroSort;

namespace SortBenchmark
{
    class Program
    {
        static void Main(string[] args)
        {
            for (var size = 1000; size <= 1000000; size *= 10)
                Run(size);
        }

        static void Run(int size)
        {
            Console.WriteLine("size: {0}", size);

            var count = 10;
            var sw = new Stopwatch();
            var arraySortTicks = 0L;
            var listSortTicks = 0L;
            var arrayOrderByTicks = 0L;
            var listOrderByTicks = 0L;
            var combSortArrayTicks = 0L;
            var combSortListTicks = 0L;
            var radixSortArrayTicks = 0L;
            var radixSortListTicks = 0L;
            var inPlaceMergeSortArrayTicks = 0L;
            var inPlaceMergeSortListTicks = 0L;
            var mergeSortArrayTicks = 0L;
            var mergeSortListTicks = 0L;
            var introSortArrayTicks = 0L;
            var introSortListTicks = 0L;
            for (var c = 0; c < count; c++)
            {
                var random = new Random(c);
                var list = Enumerable.Range(0, size)
                                     .Select(_ => random.Next(Int32.MinValue, Int32.MaxValue))
                                     .ToArray();

                // Array.Sort
                var arraySortResult = list.ToArray();
                sw.Reset();
                sw.Start();
                Array.Sort(arraySortResult);
                sw.Stop();
                arraySortTicks += sw.ElapsedTicks;

                // List<T>.Sort
                var listSortResult = list.ToList();
                sw.Reset();
                sw.Start();
                listSortResult.Sort();
                sw.Stop();
                listSortTicks += sw.ElapsedTicks;

                // Array.OrderBy
                var arrayOrderByResult = list.ToArray();
                sw.Reset();
                sw.Start();
                arrayOrderByResult = arrayOrderByResult.OrderBy(x => x).ToArray();
                sw.Stop();
                arrayOrderByTicks += sw.ElapsedTicks;

                // List<T>.OrderBy
                var listOrderByResult = list.ToList();
                sw.Reset();
                sw.Start();
                listOrderByResult = listOrderByResult.OrderBy(x => x).ToList();
                sw.Stop();
                listOrderByTicks += sw.ElapsedTicks;

                // CombSort Array
                var combSortArrayResult = list.ToArray();
                sw.Reset();
                sw.Start();
                CombSort(combSortArrayResult);
                sw.Stop();
                combSortArrayTicks += sw.ElapsedTicks;

                // CombSort List<T>
                var combSortListResult = list.ToList();
                sw.Reset();
                sw.Start();
                CombSort(combSortListResult);
                sw.Stop();
                combSortListTicks += sw.ElapsedTicks;

                // NumberRadixSorter.Sort Array
                var radixSortArrayResult = list.ToArray();
                sw.Reset();
                sw.Start();
                NumberRadixSorter.Sort(radixSortArrayResult, NumberRadixSorter.SortOrder.Ascending);
                sw.Stop();
                radixSortArrayTicks += sw.ElapsedTicks;

                // NumberRadixSorter.Sort List<T>
                var radixSortListResult = list.ToList();
                sw.Reset();
                sw.Start();
                NumberRadixSorter.Sort(radixSortListResult, NumberRadixSorter.SortOrder.Ascending);
                sw.Stop();
                radixSortListTicks += sw.ElapsedTicks;

                // InPlaceMergeSorter.Sort Array
                var inPlaceMergeSortArrayResult = list.ToArray();
                sw.Reset();
                sw.Start();
                InPlaceMergeSorter.Sort(inPlaceMergeSortArrayResult);
                sw.Stop();
                inPlaceMergeSortArrayTicks += sw.ElapsedTicks;

                // InPlaceMergeSorter.Sort List<T>
                var inPlaceMergeSortListResult = list.ToList();
                sw.Reset();
                sw.Start();
                InPlaceMergeSorter.Sort(inPlaceMergeSortListResult);
                sw.Stop();
                inPlaceMergeSortListTicks += sw.ElapsedTicks;

                // MergeSorter.Sort Array
                var mergeSortArrayResult = list.ToArray();
                sw.Reset();
                sw.Start();
                MergeSorter.Sort(mergeSortArrayResult);
                sw.Stop();
                mergeSortArrayTicks += sw.ElapsedTicks;

                // MergeSorter.Sort List<T>
                var mergeSortListResult = list.ToList();
                sw.Reset();
                sw.Start();
                MergeSorter.Sort(mergeSortListResult);
                sw.Stop();
                mergeSortListTicks += sw.ElapsedTicks;

                // IntroSorter.Sort Array
                var introSortArrayResult = list.ToArray();
                sw.Reset();
                sw.Start();
                IntroSorter.Sort(introSortArrayResult);
                sw.Stop();
                introSortArrayTicks += sw.ElapsedTicks;

                // IntroSorter.Sort List<T>
                var introSortListResult = list.ToList();
                sw.Reset();
                sw.Start();
                IntroSorter.Sort(introSortListResult);
                sw.Stop();
                introSortListTicks += sw.ElapsedTicks;


                // correctness
                if (!arraySortResult.SequenceEqual(listSortResult))
                    Console.WriteLine("!arraySortResult.SequenceEqual(listSortResult)");
                if (!arraySortResult.SequenceEqual(arrayOrderByResult))
                    Console.WriteLine("!arraySortResult.SequenceEqual(arrayOrderByResult)");
                if (!arraySortResult.SequenceEqual(listOrderByResult))
                    Console.WriteLine("!arraySortResult.SequenceEqual(listOrderByResult)");
                if (!arraySortResult.SequenceEqual(combSortArrayResult))
                    Console.WriteLine("!arraySortResult.SequenceEqual(combSortArrayResult)");
                if (!arraySortResult.SequenceEqual(combSortListResult))
                    Console.WriteLine("!arraySortResult.SequenceEqual(combSortListResult)");

                if (!arraySortResult.SequenceEqual(combSortArrayResult))
                    Console.WriteLine("!arraySortResult.SequenceEqual(combSortArrayResult)");
                if (!arraySortResult.SequenceEqual(combSortListResult))
                    Console.WriteLine("!arraySortResult.SequenceEqual(combSortListResult)");

                if (!arraySortResult.SequenceEqual(radixSortArrayResult))
                    Console.WriteLine("!arraySortResult.SequenceEqual(radixSortArrayResult)");
                if (!arraySortResult.SequenceEqual(radixSortListResult))
                    Console.WriteLine("!arraySortResult.SequenceEqual(radixSortListResult)");

                if (!arraySortResult.SequenceEqual(inPlaceMergeSortArrayResult))
                    Console.WriteLine("!arraySortResult.SequenceEqual(inPlaceMergeSortArrayResult)");
                if (!arraySortResult.SequenceEqual(inPlaceMergeSortListResult))
                    Console.WriteLine("!arraySortResult.SequenceEqual(inPlaceMergeSortListResult)");

                if (!arraySortResult.SequenceEqual(mergeSortArrayResult))
                    Console.WriteLine("!arraySortResult.SequenceEqual(mergeSortArrayResult)");
                if (!arraySortResult.SequenceEqual(mergeSortListResult))
                    Console.WriteLine("!arraySortResult.SequenceEqual(mergeSortListResult)");

                if (!arraySortResult.SequenceEqual(introSortArrayResult))
                    Console.WriteLine("!arraySortResult.SequenceEqual(introSortArrayResult)");
                if (!arraySortResult.SequenceEqual(introSortListResult))
                    Console.WriteLine("!arraySortResult.SequenceEqual(introSortListResult)");
            }

            Console.WriteLine("Array.Sort: {0} ({1:F})", arraySortTicks / count, arraySortTicks / (double)arraySortTicks);
            Console.WriteLine("List.Sort: {0} ({1:F})", listSortTicks / count, listSortTicks / (double)arraySortTicks);
            Console.WriteLine("Array.OrderBy: {0} ({1:F})", arrayOrderByTicks / count, arrayOrderByTicks / (double)arraySortTicks);
            Console.WriteLine("List.OrderBy: {0} ({1:F})", listOrderByTicks / count, listOrderByTicks / (double)arraySortTicks);

            Console.WriteLine("CombSort Array: {0} ({1:F})", combSortArrayTicks / count, combSortArrayTicks / (double)arraySortTicks);
            Console.WriteLine("CombSort List: {0} ({1:F})", combSortListTicks / count, combSortListTicks / (double)arraySortTicks);

            Console.WriteLine("NumberRadixSorter.Sort Array: {0} ({1:F})", radixSortArrayTicks / count, radixSortArrayTicks / (double)arraySortTicks);
            Console.WriteLine("NumberRadixSorter.Sort List: {0} ({1:F})", radixSortListTicks / count, radixSortListTicks / (double)arraySortTicks);

            Console.WriteLine("InPlaceMergeSorter.Sort Array: {0} ({1:F})", inPlaceMergeSortArrayTicks / count, inPlaceMergeSortArrayTicks / (double)arraySortTicks);
            Console.WriteLine("InPlaceMergeSorter.Sort List: {0} ({1:F})", inPlaceMergeSortListTicks / count, inPlaceMergeSortListTicks / (double)arraySortTicks);

            Console.WriteLine("MergeSorter.Sort Array: {0} ({1:F})", mergeSortArrayTicks / count, mergeSortArrayTicks / (double)arraySortTicks);
            Console.WriteLine("MergeSorter.Sort List: {0} ({1:F})", mergeSortListTicks / count, mergeSortListTicks / (double)arraySortTicks);

            Console.WriteLine("IntroSorter.Sort Array: {0} ({1:F})", introSortArrayTicks / count, introSortArrayTicks / (double)arraySortTicks);
            Console.WriteLine("IntroSorter.Sort List: {0} ({1:F})", introSortListTicks / count, introSortListTicks / (double)arraySortTicks);

            Console.WriteLine();
        }


        static void CombSort(IList<Int32> list)
        {
            const double shrinkFactor = 1.247330950103979; // 1.0 / (1.0 - 1.0 / Math.Pow(Math.E, Math.PI));
            var gap = list.Count;
            var swapped = true;
            while (gap > 1 || swapped)
            {
                if (gap > 1)
                    gap = (int)(gap / shrinkFactor);
                swapped = false;
                for (var n = 0; n + gap < list.Count; n++)
                {
                    if (list[n].CompareTo(list[n + gap]) > 0)
                    {
                        var t = list[n];
                        list[n] = list[n + gap];
                        list[n + gap] = t;
                        swapped = true;
                    }
                }
            }
        }
    }
}
