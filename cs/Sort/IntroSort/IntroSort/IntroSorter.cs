﻿// Copyright (c) 2009, ksksts <http://ksksts.blogspot.com/>
// Licensed under the zlib/libpng: http://www.opensource.org/licenses/zlib-license.php
// Inspired by STLport <http://stlport.sourceforge.net/>
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace IntroSort
{
    public class IntroSorter
    {
        public static void Sort<T>(IList<T> list)
        {
            var comparer = Comparer<T>.Default;
            if (comparer == null)
                throw new InvalidOperationException("The default comparer Comparer<T>.Default cannot find an implementation of the IComparable<T> generic interface or the IComparable interface for type T.");
            Sort(list, 0, list.Count, comparer.Compare);
        }

        public static void Sort<T>(IList<T> list, IComparer<T> comparer)
        {
            Sort(list, 0, list.Count, comparer.Compare);
        }

        public static void Sort<T>(IList<T> list, Comparison<T> comparison)
        {
            Sort(list, 0, list.Count, comparison);
        }

        public static void Sort<T>(IList<T> list, int index, int count, Comparison<T> comparison)
        {
            var sorter = new Sorter<T>();
            sorter.Sort(list, index, count, comparison);
        }


        private class Sorter<T>
        {
            private IList<T> list;
            private Comparison<T> comparison;
            private readonly int threshold = 16;

            public Sorter() { }

            public void Sort(IList<T> list, int index, int count, Comparison<T> comparison)
            {
                this.list = list;
                this.comparison = comparison;
                if (count == 0)
                    return;
                IntroSortLoop(index, index + count, Log(count));
                FinalInsertionSort(index, index + count);
            }

            private void IntroSortLoop(int first, int last, int depthLimit)
            {
                Debug.Assert(first <= last);
                while (last - first > threshold)
                {
                    if (depthLimit == 0)
                    {
                        PartialSort(first, last, last);
                        return;
                    }
                    depthLimit--;
                    var median = Median(list[first], list[(first + last) / 2], list[last - 1]);
                    var cut = UnguardedPartition(first, last, median);
                    IntroSortLoop(cut, last, depthLimit);
                    last = cut;
                }
            }

            private int UnguardedPartition(int first, int last, T pivot)
            {
                Debug.Assert(first <= last);
                while (true)
                {
                    while (comparison(list[first], pivot) < 0)
                        first++;
                    last--;
                    while (comparison(pivot, list[last]) < 0)
                        last--;
                    if (!(first < last))
                        return first;
                    var tv = list[first];
                    list[first] = list[last];
                    list[last] = tv;
                    first++;
                }
            }

            private void PartialSort(int first, int middle, int last)
            {
                Debug.Assert(first <= middle);
                Debug.Assert(middle <= last);
                MakeHeap(first, middle);
                for (var n = middle; n < last; n++)
                    if (comparison(list[n], list[first]) < 0)
                        PopHeap(first, middle, n, list[n]);
                SortHeap(first, middle);
            }

            private void MakeHeap(int first, int last)
            {
                Debug.Assert(first <= last);
                var count = last - first;
                if (count < 2)
                    return;
                var parent = (count - 2) / 2;
                while (true)
                {
                    AdjustHeap(first, parent, count, list[first + parent]);
                    if (parent == 0)
                        return;
                    parent--;
                }
            }

            private void AdjustHeap(int first, int hole, int count, T value)
            {
                var top = hole;
                var second = 2 * hole + 2;
                while (second < count)
                {
                    if (comparison(list[first + second], list[first + second - 1]) < 0)
                        second--;
                    list[first + hole] = list[first + second];
                    hole = second;
                    second = 2 * (second + 1);
                }
                if (second == count)
                {
                    list[first + hole] = list[first + second - 1];
                    hole = second - 1;
                }
                PushHeap(first, hole, top, value);
            }

            private void PushHeap(int first, int hole, int top, T value)
            {
                var parent = (hole - 1) / 2;
                while (hole > top && comparison(list[first + parent], value) < 0)
                {
                    list[first + hole] = list[first + parent];
                    hole = parent;
                    parent = (hole - 1) / 2;
                }
                list[first + hole] = value;
            }

            private void PopHeap(int first, int last, int result, T value)
            {
                Debug.Assert(first <= last);
                list[result] = list[first];
                AdjustHeap(first, 0, last - first, value);
            }

            private void PopHeap(int first, int last)
            {
                Debug.Assert(first <= last);
                PopHeap(first, last - 1, last - 1, list[last - 1]);
            }

            private void SortHeap(int first, int last)
            {
                Debug.Assert(first <= last);
                while (last - first > 1)
                    PopHeap(first, last--);
            }

            private void FinalInsertionSort(int first, int last)
            {
                Debug.Assert(first <= last);
                if (last - first > threshold)
                {
                    InsertionSort(first, first + threshold);
                    UnguardedInsertionSort(first + threshold, last);
                }
                else
                    InsertionSort(first, last);
            }

            private void InsertionSort(int first, int last)
            {
                Debug.Assert(first <= last);
                for (var n = first + 1; n < last; n++)
                {
                    for (var m = n; m > first; m--)
                    {
                        if (comparison(list[m], list[m - 1]) < 0)
                        {
                            var tv = list[m];
                            list[m] = list[m - 1];
                            list[m - 1] = tv;
                        }
                        else
                            break;
                    }
                }
            }

            private void UnguardedInsertionSort(int first, int last)
            {
                Debug.Assert(first <= last);
                for (var n = first; n < last; n++)
                {
                    T val = list[n];
                    var m = n - 1;
                    Debug.Assert(m >= 0);
                    while (comparison(val, list[m]) < 0)
                    {
                        list[m + 1] = list[m];
                        m--;
                        Debug.Assert(m >= 0);
                    }
                    list[m + 1] = val;
                }
            }

            private T Median(T x, T y, T z)
            {
                if (comparison(x, y) < 0)
                    if (comparison(y, z) < 0)
                        return y;
                    else if (comparison(x, z) < 0)
                        return z;
                    else
                        return x;
                else if (comparison(x, z) < 0)
                    return x;
                else if (comparison(y, z) < 0)
                    return z;
                else
                    return y;
            }


            private static int Log(int x)
            {
                var log = 0;
                for (log = 0; x != 1; x >>= 1)
                    log++;
                return log;
            }
        }
    }
}
