﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using MergeSort;

namespace Benchmark
{
    class Program
    {
        static void Main(string[] args)
        {
            for (var size = 1000; size <= 1000000; size *= 10)
                Run(size);
        }

        static void Run(int size)
        {
            Console.WriteLine("size: {0}", size);

            var count = 10;
            var sw = new Stopwatch();
            var inPlaceMergeSortTicks = 0L;
            var mergeSortTicks = 0L;
            var arraySortTicks = 0L;
            for (var c = 0; c < count; c++)
            {
                var random = new Random(c);
                var list = Enumerable.Range(0, size)
                                     .Select(_ => random.Next(Int32.MinValue, Int32.MaxValue))
                                     .ToArray();

                // in place merge sort
                var inPlaceMergeSortResult = (IList<Int32>)list.Clone();
                sw.Reset();
                sw.Start();
                InPlaceMergeSorter.Sort(inPlaceMergeSortResult);
                sw.Stop();
                inPlaceMergeSortTicks += sw.ElapsedTicks;

                // merge sort
                var mergeSortResult = (IList<Int32>)list.Clone();
                sw.Reset();
                sw.Start();
                MergeSorter.Sort(mergeSortResult);
                sw.Stop();
                mergeSortTicks += sw.ElapsedTicks;

                // Array.Sort
                var arraySortResult = list.ToArray();
                sw.Reset();
                sw.Start();
                Array.Sort(arraySortResult);
                sw.Stop();
                arraySortTicks += sw.ElapsedTicks;

                // correctness
                for (var n = 0; n < size; n++)
                {
                    if (inPlaceMergeSortResult[n] != arraySortResult[n])
                        Console.WriteLine("inPlaceMergeSortResult[{0}]: {1}, arraySortResult[{0}]: {2}", n, inPlaceMergeSortResult[n], arraySortResult[n]);
                    if (mergeSortResult[n] != arraySortResult[n])
                        Console.WriteLine("mergeSortResult[{0}]: {1}, arraySortResult[{0}]: {2}", n, mergeSortResult[n], arraySortResult[n]);
                }
            }

            Console.WriteLine("InPlaceMergeSorter.Sort: {0} ({1:F})", inPlaceMergeSortTicks / count, inPlaceMergeSortTicks / (double)arraySortTicks);
            Console.WriteLine("MergeSorter.Sort: {0} ({1:F})", mergeSortTicks / count, mergeSortTicks / (double)arraySortTicks);
            Console.WriteLine("Array.Sort: {0} ({1:F})", arraySortTicks / count, arraySortTicks / (double)arraySortTicks);
        }
    }
}
