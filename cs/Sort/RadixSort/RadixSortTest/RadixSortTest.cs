﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RadixSort;

namespace RadixSortTest
{
    [TestClass]
    public class RadixSortTest
    {
        struct Pair<TFirst, TSecond>
            : IComparable<Pair<TFirst, TSecond>>
            where TFirst : IComparable<TFirst>
            where TSecond : IComparable<TSecond>
        {
            public TFirst First { get; set; }
            public TSecond Second { get; set; }
            public Pair(TFirst first, TSecond second)
                : this()
            {
                First = first;
                Second = second;
            }
            public int CompareTo(Pair<TFirst, TSecond> x)
            {
                var result = 0;
                result = First.CompareTo(x.First);
                if (result == 0)
                    result = Second.CompareTo(x.Second);
                return result;
            }
            public override string ToString()
            {
                return string.Format("{0}, {1}", First, Second);
            }
        }


        public RadixSortTest() { }

        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        public const int RANDOM_TEST_COUNT = 32;

        [TestMethod]
        public void TestSortUInt32Random()
        {
            for (int count = 0; count < RANDOM_TEST_COUNT; count++)
            {
                var random = new Random(count);
                var list = Enumerable.Range(0, random.Next(UInt16.MaxValue))
                                     .Select(_ => new Pair<UInt32, UInt32>((UInt32)random.Next(Int32.MinValue, Int32.MaxValue), 
                                                                           (UInt32)random.Next(Int32.MinValue, Int32.MaxValue)))
                                     .ToArray();
                // ascending
                {
                    var expected = (Pair<UInt32, UInt32>[])list.Clone();
                    Array.Sort(expected);
                    var result = (Pair<UInt32, UInt32>[])list.Clone();
                    RadixSorter.Sort(result, x => x.Second, RadixSorter.SortOrder.Ascending);
                    RadixSorter.Sort(result, x => x.First, RadixSorter.SortOrder.Ascending);
                    CollectionAssert.AreEqual(expected, result);
                }
                // descending
                {
                    var expected = (Pair<UInt32, UInt32>[])list.Clone();
                    Array.Sort(expected);
                    Array.Reverse(expected);
                    var result = (Pair<UInt32, UInt32>[])list.Clone();
                    RadixSorter.Sort(result, x => x.Second, RadixSorter.SortOrder.Descending);
                    RadixSorter.Sort(result, x => x.First, RadixSorter.SortOrder.Descending);
                    CollectionAssert.AreEqual(expected, result);
                }
            }
        }

        [TestMethod]
        public void TestSortUInt64Random()
        {
            for (int count = 0; count < RANDOM_TEST_COUNT; count++)
            {
                var random = new Random(count);
                var list = Enumerable.Range(0, random.Next(UInt16.MaxValue))
                                     .Select(_ => new Pair<UInt64, UInt64>((UInt64)((random.NextDouble() - 0.5) * 2.0 * Int64.MinValue),
                                                                           (UInt64)((random.NextDouble() - 0.5) * 2.0 * Int64.MinValue)))
                                     .ToArray();
                // ascending
                {
                    var expected = (Pair<UInt64, UInt64>[])list.Clone();
                    Array.Sort(expected);
                    var result = (Pair<UInt64, UInt64>[])list.Clone();
                    RadixSorter.Sort(result, x => x.Second, RadixSorter.SortOrder.Ascending);
                    RadixSorter.Sort(result, x => x.First, RadixSorter.SortOrder.Ascending);
                    CollectionAssert.AreEqual(expected, result);
                }
                // descending
                {
                    var expected = (Pair<UInt64, UInt64>[])list.Clone();
                    Array.Sort(expected);
                    Array.Reverse(expected);
                    var result = (Pair<UInt64, UInt64>[])list.Clone();
                    RadixSorter.Sort(result, x => x.Second, RadixSorter.SortOrder.Descending);
                    RadixSorter.Sort(result, x => x.First, RadixSorter.SortOrder.Descending);
                    CollectionAssert.AreEqual(expected, result);
                }
            }
        }

        [TestMethod]
        public void TestSortInt32Random()
        {
            for (int count = 0; count < RANDOM_TEST_COUNT; count++)
            {
                var random = new Random(count);
                var list = Enumerable.Range(0, random.Next(UInt16.MaxValue))
                                     .Select(_ => new Pair<Int32, Int32>(random.Next(Int32.MinValue, Int32.MaxValue), 
                                                                         random.Next(Int32.MinValue, Int32.MaxValue)))
                                     .ToArray();
                // ascending
                {
                    var expected = (Pair<Int32, Int32>[])list.Clone();
                    Array.Sort(expected);
                    var result = (Pair<Int32, Int32>[])list.Clone();
                    RadixSorter.Sort(result, x => x.Second, RadixSorter.SortOrder.Ascending);
                    RadixSorter.Sort(result, x => x.First, RadixSorter.SortOrder.Ascending);
                    CollectionAssert.AreEqual(expected, result);
                }
                // descending
                {
                    var expected = (Pair<Int32, Int32>[])list.Clone();
                    Array.Sort(expected);
                    Array.Reverse(expected);
                    var result = (Pair<Int32, Int32>[])list.Clone();
                    RadixSorter.Sort(result, x => x.Second, RadixSorter.SortOrder.Descending);
                    RadixSorter.Sort(result, x => x.First, RadixSorter.SortOrder.Descending);
                    CollectionAssert.AreEqual(expected, result);
                }
            }
        }

        [TestMethod]
        public void TestSortInt64Random()
        {
            for (int count = 0; count < RANDOM_TEST_COUNT; count++)
            {
                var random = new Random(count);
                var list = Enumerable.Range(0, random.Next(UInt16.MaxValue))
                                     .Select(_ => new Pair<Int64, Int64>((Int64)((random.NextDouble() - 0.5) * 2.0 * Int64.MinValue),
                                                                         (Int64)((random.NextDouble() - 0.5) * 2.0 * Int64.MinValue)))
                                     .ToArray();
                // ascending
                {
                    var expected = (Pair<Int64, Int64>[])list.Clone();
                    Array.Sort(expected);
                    var result = (Pair<Int64, Int64>[])list.Clone();
                    RadixSorter.Sort(result, x => x.Second, RadixSorter.SortOrder.Ascending);
                    RadixSorter.Sort(result, x => x.First, RadixSorter.SortOrder.Ascending);
                    CollectionAssert.AreEqual(expected, result);
                }
                // descending
                {
                    var expected = (Pair<Int64, Int64>[])list.Clone();
                    Array.Sort(expected);
                    Array.Reverse(expected);
                    var result = (Pair<Int64, Int64>[])list.Clone();
                    RadixSorter.Sort(result, x => x.Second, RadixSorter.SortOrder.Descending);
                    RadixSorter.Sort(result, x => x.First, RadixSorter.SortOrder.Descending);
                    CollectionAssert.AreEqual(expected, result);
                }
            }
        }

        [TestMethod]
        public void TestSortSingleRandom()
        {
            for (int count = 0; count < RANDOM_TEST_COUNT; count++)
            {
                var random = new Random(count);
                var list = Enumerable.Range(0, random.Next(UInt16.MaxValue))
                                     .Select(_ => new Pair<Single, Single>((Single)((random.NextDouble() - 0.5) * 2.0),
                                                                           (Single)((random.NextDouble() - 0.5) * 2.0)))
                                     .ToArray();
                // ascending
                {
                    var expected = (Pair<Single, Single>[])list.Clone();
                    Array.Sort(expected);
                    var result = (Pair<Single, Single>[])list.Clone();
                    RadixSorter.Sort(result, x => x.Second, RadixSorter.SortOrder.Ascending);
                    RadixSorter.Sort(result, x => x.First, RadixSorter.SortOrder.Ascending);
                    CollectionAssert.AreEqual(expected, result);
                }
                // descending
                {
                    var expected = (Pair<Single, Single>[])list.Clone();
                    Array.Sort(expected);
                    Array.Reverse(expected);
                    var result = (Pair<Single, Single>[])list.Clone();
                    RadixSorter.Sort(result, x => x.Second, RadixSorter.SortOrder.Descending);
                    RadixSorter.Sort(result, x => x.First, RadixSorter.SortOrder.Descending);
                    CollectionAssert.AreEqual(expected, result);
                }
            }
        }

        [TestMethod]
        public void TestSortDoubleRandom()
        {
            for (int count = 0; count < RANDOM_TEST_COUNT; count++)
            {
                var random = new Random(count);
                var list = Enumerable.Range(0, random.Next(UInt16.MaxValue))
                                     .Select(_ => new Pair<Double, Double>((random.NextDouble() - 0.5) * 2.0,
                                                                           (random.NextDouble() - 0.5) * 2.0))
                                     .ToArray();
                // ascending
                {
                    var expected = (Pair<Double, Double>[])list.Clone();
                    Array.Sort(expected);
                    var result = (Pair<Double, Double>[])list.Clone();
                    RadixSorter.Sort(result, x => x.Second, RadixSorter.SortOrder.Ascending);
                    RadixSorter.Sort(result, x => x.First, RadixSorter.SortOrder.Ascending);
                    CollectionAssert.AreEqual(expected, result);
                }
                // descending
                {
                    var expected = (Pair<Double, Double>[])list.Clone();
                    Array.Sort(expected);
                    Array.Reverse(expected);
                    var result = (Pair<Double, Double>[])list.Clone();
                    RadixSorter.Sort(result, x => x.Second, RadixSorter.SortOrder.Descending);
                    RadixSorter.Sort(result, x => x.First, RadixSorter.SortOrder.Descending);
                    CollectionAssert.AreEqual(expected, result);
                }
            }
        }
    }
}

    