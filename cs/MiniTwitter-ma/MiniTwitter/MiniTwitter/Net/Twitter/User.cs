﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MiniTwitter.Net.Twitter
{
    [Serializable]
    [XmlRoot("user")]
    public class User : PropertyChangedBase, IEquatable<User>
    {
        private int id;

        [XmlElement("id")]
        public int ID
        {
            get { return id; }
            set
            {
                if (id != value)
                {
                    id = value;
                    OnPropertyChanged("ID");
                }
            }
        }

        private string name;

        [XmlElement("name")]
        public string Name
        {
            get { return name; }
            set
            {
                if (name != value)
                {
                    name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        private string screenName;

        [XmlElement("screen_name")]
        public string ScreenName
        {
            get { return screenName; }
            set
            {
                if (screenName != value)
                {
                    screenName = value;
                    OnPropertyChanged("ScreenName");
                }
            }
        }

        private string location;

        [XmlElement("location")]
        public string Location
        {
            get { return location; }
            set
            {
                if (location != value)
                {
                    location = value;
                    OnPropertyChanged("Location");
                }
            }
        }

        private string description;

        [XmlElement("description")]
        public string Description
        {
            get { return description; }
            set
            {
                if (description != value)
                {
                    description = value;
                    OnPropertyChanged("Description");
                }
            }
        }

        private string imageUrl;

        [XmlElement("profile_image_url")]
        public string ImageUrl
        {
            get { return imageUrl; }
            set
            {
                if (imageUrl != value)
                {
                    imageUrl = value;
                    OnPropertyChanged("ImageUrl");
                }
            }
        }

        private string url;

        [XmlElement("url")]
        public string Url
        {
            get { return url; }
            set
            {
                if (url != value)
                {
                    url = value;
                    OnPropertyChanged("Url");
                }
            }
        }

        private bool _protected;

        [XmlElement("protected")]
        public bool Protected
        {
            get { return _protected; }
            set
            {
                if (_protected != value)
                {
                    _protected = value;
                    OnPropertyChanged("Protected");
                }
            }
        }

        private Status status;

        [XmlElement("status")]
        public Status Status
        {
            get { return status; }
            set
            {
                if (status != value)
                {
                    status = value;
                    OnPropertyChanged("Status");
                }
            }
        }

        public bool Equals(User other)
        {
            if (other == null)
            {
                throw new ArgumentNullException("other");
            }
            return (this.ID == other.ID);
        }

        public override int GetHashCode()
        {
            return this.ID;
        }
    }
}
