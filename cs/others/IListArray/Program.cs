﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace IListArray
{
    class Program
    {
        static void Main(string[] args)
        {
            var sw = new Stopwatch();
            var arrayTicks = 0L;
            var ilistArrayTicks = 0L;
            var listTicks = 0L;
            var ilistListTicks = 0L;
            var ilistArrayWrapperTicks = 0L;
            var count = 100;

            for (var n = 0; n < count; n++)
            {
                var random = new Random(n);
                var source = Enumerable.Range(0, random.Next(short.MaxValue))
                                      .Select(_ => random.Next(int.MinValue, int.MaxValue))
                                      .ToArray();
                var indices = Enumerable.Range(0, source.Length)
                                        .Select(_ => random.Next(source.Length))
                                        .ToArray();

                // array
                var array = source.ToArray();
                var arrayResult = 0;
                sw.Reset();
                sw.Start();
                foreach (var i in indices)
                    arrayResult = unchecked(arrayResult + array[i]);
                sw.Stop();
                arrayTicks += sw.ElapsedTicks;

                // (IList<T>)array
                var ilistArray = (IList<int>)source.ToArray();
                var ilistArrayResult = 0;
                sw.Reset();
                sw.Start();
                foreach (var i in indices)
                    ilistArrayResult = unchecked(ilistArrayResult + ilistArray[i]);
                sw.Stop();
                ilistArrayTicks += sw.ElapsedTicks;

                // list
                var list = source.ToList();
                var listResult = 0;
                sw.Reset();
                sw.Start();
                foreach (var i in indices)
                    listResult = unchecked(listResult + list[i]);
                sw.Stop();
                listTicks += sw.ElapsedTicks;

                // (IList<T>)list
                var ilistList = (IList<int>)source.ToList();
                var ilistListResult = 0;
                sw.Reset();
                sw.Start();
                foreach (var i in indices)
                    ilistListResult = unchecked(ilistListResult + ilistList[i]);
                sw.Stop();
                ilistListTicks += sw.ElapsedTicks;

                // IListArrayWrapper
                var ilistArrayWrapper = new IListArrayWrapper(source.ToArray());
                var ilistArrayWrapperResult = 0;
                sw.Reset();
                sw.Start();
                foreach (var i in indices)
                    ilistArrayWrapperResult = unchecked(ilistArrayWrapperResult + ilistArrayWrapper[i]);
                sw.Stop();
                ilistArrayWrapperTicks += sw.ElapsedTicks;

                // correctness
                if (arrayResult != ilistArrayResult)
                    Console.WriteLine("arrayResult != ilistArrayResult");
                if (arrayResult != listResult)
                    Console.WriteLine("arrayResult != listResult");
                if (arrayResult != ilistListResult)
                    Console.WriteLine("arrayResult != ilistListResult");
                if (arrayResult != ilistArrayWrapperResult)
                    Console.WriteLine("arrayResult != ilistArrayWrapperResult");
            }

            Console.WriteLine("array: {0} ({1:F})", arrayTicks / count, arrayTicks / (double)arrayTicks);
            Console.WriteLine("(IList<T>)array: {0} ({1:F})", ilistArrayTicks / count, ilistArrayTicks / (double)arrayTicks);
            Console.WriteLine("list: {0} ({1:F})", listTicks / count, listTicks / (double)arrayTicks);
            Console.WriteLine("(IList<T>)list: {0} ({1:F})", ilistListTicks / count, ilistListTicks / (double)arrayTicks);
            Console.WriteLine("IListArrayWrapper: {0} ({1:F})", ilistArrayWrapperTicks / count, ilistArrayWrapperTicks / (double)arrayTicks);
        }
    }
}
