; 2010年分（平成22年分）「【確定申告書作成コーナー】-配当所得、配当控除(上場株式等)」入力スクリプト
; 【確定申告書作成コーナー】-配当所得、配当控除(上場株式等)」 https://www.keisan.nta.go.jp/h22/syotoku/ta_subX5b.jsp
; AutoIt v3 (v3.3.6.1), Windows 7 Pro. 64bit, InternetExplorer 8
; 2011/01/22 ksksts@gmail.com
#include <Array.au3>
#include <Date.au3>
#include <File.au3>
#include <IE.au3>

;$filename = "etax-dividend-sample.txt"  ; 配当データのファイル名（タブ区切りテキストファイル）
$filename = "etax-dividend-2010.txt"  ; 配当データのファイル名（タブ区切りテキストファイル）

; 配当データを読み込む
$records = ReadTSVFile($filename)

; 「配当所得、配当控除(上場株式等)」ページのIEオブジェクトを取得する
$oIE = _IEAttach ("【確定申告書作成コーナー】-配当所得、配当控除(上場株式等)")
; 配当を入力する
For $n = 1 To $records[0]
	InputDividend($oIE, $records[$n])
	; もう一件入力する onclick="AddNew(); return false;"
	_IEImgClick($oIE, "img/ta_Ins_rec.gif")
	ConsoleWrite($n & ": " & _ArrayToString($records[$n]) & " (" & _Now() & ")" & @CRLF)
Next

Exit


; タブ区切りテキストファイルの内容を読み込み配列に格納する（配列の先頭要素は配列の要素数）
Func ReadTSVFile($filename)
	Dim $records
	If Not _FileReadToArray($filename, $records) Then
	   MsgBox(4096, "Error", " Error reading log to Array  error:" & @error)
	   Exit
	EndIf
	For $n = 1 To $records[0]
		$records[$n] = StringSplit($records[$n], @TAB)
	Next
	;~ For $n = 1 To $records[0]
	;~ 	ConsoleWrite(_ArrayToString($records[$n]) & @CRLF)
	;~ Next
	return $records
EndFunc

; 配当を入力する
Func InputDividend($oIE, $record)
	; 配列の要素数が少ない場合は空の要素を追加する
	If UBound($record) < 13 Then
		ReDim $record[13]
	EndIf

	; 配当の入力

	; (１) 支払通知書の種類（書面提出かつ分離課税の場合は入力不要です。）

	; イ 支払通知書の種類を選択してください。 onChange="jsSetList2();"
	; 1: 「１ 上場株式配当等の支払通知書」
	; 2: 「２ オープン型証券投資信託収益の分配の支払通知書」
	; 3: 「３ 配当等とみなす金額に関する支払通知書」
	$oSelect = _IEGetObjById($oIE, "sP_2")
	_IEFormElementOptionSelect ($oSelect, $record[1])
	; ロ 支払通知書の内容のうち、以下の事項について選択してください。
	; 外貨建資産割合 onChange="jsSetList3();"
	; 1: 「１ 記載無し又は50%以下」
	; 2: 「２ 50%超75%以下」
	; 3: 「３ 75%超」
	$oSelect = _IEGetObjById($oIE, "sP_3")
	_IEFormElementOptionSelect ($oSelect, $record[2])
	; 非株式割合 onChange="jsSetList4();"
	; 1: 「１ 記載無し又は50%以下」
	; 2: 「２ 50%超75%以下」
	; 3: 「３ 75%超」
	$oSelect = _IEGetObjById($oIE, "sP_4")
	_IEFormElementOptionSelect ($oSelect, $record[3])

	; (２) 配当等の種類
	; 1: 「１ 上場株式等に係る配当等(次の２〜４に該当するものを除く。)」
	; 2: 「２ 外貨建等証券投資信託以外の特定証券投資信託の収益の分配」
	; 3: 「３ 外貨建等証券投資信託の収益の分配（特定外貨建等証券投資信託以外）」
	; 4: 「４ 配当控除(税額控除)の対象とならない配当等」
	$oForm = _IEGetObjByName($oIE, "SubForm") 
	_IEFormElementRadioSelect ($oForm, $record[4], "sP_5")

	; (３) 種目(全角5文字以内) onBlur="jsSetList6();"
	$oText = _IEGetObjById($oIE, "sP_6")
	_IEFormElementSetValue ($oText, $record[5])
	$oText.fireEvent("OnBlur")

	; (４) 銘柄等(全角28文字以内) onBlur="jsSetList7();"
	$oText = _IEGetObjById($oIE, "sP_7")
	_IEFormElementSetValue ($oText, $record[6])
	$oText.fireEvent("OnBlur")

	; (５) 支払の取扱者の名称等(全角28文字以内) onBlur="jsSetList8();"
	$oText = _IEGetObjById($oIE, "sP_8")
	_IEFormElementSetValue ($oText, $record[7])
	$oText.fireEvent("OnBlur")

	; (６) 収入金額 onBlur="jsIventAddComma(this,11);jsSetList9();"
	$oText = _IEGetObjById($oIE, "sP_9")
	_IEFormElementSetValue ($oText, $record[8])
	$oText.fireEvent("OnBlur")

	; (７) 源泉徴収税額(所得税) onBlur="jsIventAddComma(this,11);jsSetList10();"
	$oText = _IEGetObjById($oIE, "sP_10")
	_IEFormElementSetValue ($oText, $record[9])
	$oText.fireEvent("OnBlur")
	; 内　未納付の源泉徴収税額 onBlur="jsIventAddComma(this,11);jsSetList11();"
	$oText = _IEGetObjById($oIE, "sP_11")
	_IEFormElementSetValue ($oText, $record[10])
	$oText.fireEvent("OnBlur")

	; (８) 配当割額控除額(住民税) onBlur="jsIventAddComma(this,11);jsSetList12();"
	$oText = _IEGetObjById($oIE, "sP_12")
	_IEFormElementSetValue ($oText, $record[11])
	$oText.fireEvent("OnBlur")

	; (９) 必要経費 onBlur="jsIventAddComma(this,11);jsSetList13();"
	$oText = _IEGetObjById($oIE, "sP_13")
	_IEFormElementSetValue ($oText, $record[12])
	$oText.fireEvent("OnBlur")
EndFunc
