﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MSTestCustomAssertion
{
    [TestClass]
    public class ExpressionAssertTest
    {
        public ExpressionAssertTest() { }

        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        [TestMethod]
        public void TestIsTrue()
        {
            ExpressionAssert.IsTrue(() => false);
        }
    }
}
