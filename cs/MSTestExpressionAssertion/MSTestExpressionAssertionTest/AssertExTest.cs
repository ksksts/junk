﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestExpressionAssertion;

namespace MSTestExpressionAssertionTest
{
    [TestClass]
    public class AssertExTest
    {
        public AssertExTest() { }

        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        [TestMethod]
        public void TestIsTrue()
        {
            try
            {
                AssertEx.IsTrue(() => true);
            }
            catch (AssertExFailedException)
            {
                Assert.Fail();
            }

            try
            {
                AssertEx.IsTrue(() => false);
                Assert.Fail();
            }
            catch (AssertExFailedException ex)
            {
                var msg = string.Join(Environment.NewLine,
                                      new[] { "AssertEx.IsTrue failed.", 
                                              "false: False", });
                Assert.AreEqual(msg, ex.Message);
                Assert.IsFalse(ex.StackTrace.Contains("MSTestExpressionAssertion."));
            }

            try
            {
                AssertEx.IsTrue(() => false, "message");
                Assert.Fail();
            }
            catch (AssertExFailedException ex)
            {
                var msg = string.Join(Environment.NewLine,
                                      new[] { "AssertEx.IsTrue failed.", 
                                              "message", 
                                              "false: False", });
                Assert.AreEqual(msg, ex.Message);
                Assert.IsFalse(ex.StackTrace.Contains("MSTestExpressionAssertion."));
            }

            try
            {
                AssertEx.IsTrue(() => false, "message, {0}", "parameter");
                Assert.Fail();
            }
            catch (AssertExFailedException ex)
            {
                var msg = string.Join(Environment.NewLine,
                                      new[] { "AssertEx.IsTrue failed.", 
                                              "message, parameter", 
                                              "false: False", });
                Assert.AreEqual(msg, ex.Message);
                Assert.IsFalse(ex.StackTrace.Contains("MSTestExpressionAssertion."));
            }

            try
            {
                AssertEx.IsTrue(() => string.Format(null) == "");
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            }
        }

        [TestMethod]
        public void TestIsFalse()
        {
            try
            {
                AssertEx.IsFalse(() => false);
            }
            catch (AssertExFailedException)
            {
                Assert.Fail();
            }

            try
            {
                AssertEx.IsFalse(() => true);
                Assert.Fail();
            }
            catch (AssertExFailedException ex)
            {
                var msg = string.Join(Environment.NewLine,
                                      new[] { "AssertEx.IsFalse failed.", 
                                              "true: True", });
                Assert.AreEqual(msg, ex.Message);
                Assert.IsNull(ex.InnerException);
                Assert.IsFalse(ex.StackTrace.Contains("MSTestExpressionAssertion."));
            }

            try
            {
                AssertEx.IsFalse(() => true, "message");
                Assert.Fail();
            }
            catch (AssertExFailedException ex)
            {
                var msg = string.Join(Environment.NewLine,
                                      new[] { "AssertEx.IsFalse failed.", 
                                              "message", 
                                              "true: True", });
                Assert.AreEqual(msg, ex.Message);
                Assert.IsFalse(ex.StackTrace.Contains("MSTestExpressionAssertion."));
            }

            try
            {
                AssertEx.IsFalse(() => true, "message, {0}", "parameter");
                Assert.Fail();
            }
            catch (AssertExFailedException ex)
            {
                var msg = string.Join(Environment.NewLine,
                                      new[] { "AssertEx.IsFalse failed.", 
                                              "message, parameter", 
                                              "true: True", });
                Assert.AreEqual(msg, ex.Message);
                Assert.IsFalse(ex.StackTrace.Contains("MSTestExpressionAssertion."));
            }

            try
            {
                AssertEx.IsFalse(() => string.Format(null) == "");
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            }
        }
    }
}
