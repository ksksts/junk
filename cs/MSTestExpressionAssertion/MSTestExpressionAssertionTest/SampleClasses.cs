﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSTestExpressionAssertionTest
{
    public struct Point
    {
        public double X { get; set; }
        public double Y { get; set; }

        public static readonly Point ORIGIN = new Point(0.0, 0.0);

        public Point(double x, double y)
            : this()
        {
            X = x;
            Y = y;
        }

        public Point(Point p, Vector v)
            : this(p.X + v.X, p.Y + v.Y)
        {
        }

        public double CalculateDistance(Point p)
        {
            double dx = X - p.X;
            double dy = Y - p.Y;
            return Math.Sqrt(dx * dx + dy * dy);
        }

        public override string ToString()
        {
            return string.Format("({0:R}, {1:R})", X, Y);
        }

        public static Point Interpolate(Point x, Point y, double p)
        {
            double cp = 1.0 - p;
            return new Point(x.X * cp + y.X * p, x.Y * cp + y.Y * p);
        }
    }

    public struct Vector
    {
        public double X { get; set; }
        public double Y { get; set; }

        public double Norm
        {
            get { return Math.Sqrt(X * X + Y * Y); }
        }

        public Vector(double x, double y)
            : this()
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return string.Format("({0:R}, {1:R})", X, Y);
        }
    }

    public struct Matrix
    {
        public double M00 { get; set; }
        public double M01 { get; set; }
        public double M10 { get; set; }
        public double M11 { get; set; }

        public Matrix(double m00, double m01,
                      double m10, double m11)
            : this()
        {
            M00 = m00; M01 = m01;
            M10 = m10; M01 = m11;
        }

        public Point Transform(Point p)
        {
            return new Point(M00 * p.X + M01 * p.Y,
                             M10 * p.X + M11 * p.Y);
        }

        public Vector Transform(Vector v)
        {
            return new Vector(M00 * v.X + M01 * v.Y,
                              M10 * v.X + M11 * v.Y);
        }

        public override string ToString()
        {
            return string.Format("({0:R}, {1:R}, ", M00, M01) + Environment.NewLine +
                   string.Format(" {0:R}, {1:R})", M10, M11);
        }

        public static Matrix MakeRotation(double angle)
        {
            double sin = Math.Sin(angle);
            double cos = Math.Cos(angle);
            return new Matrix(cos, -sin,
                              sin, cos);
        }
    }
}
